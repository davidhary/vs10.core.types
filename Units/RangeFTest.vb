﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports isr.Core.Types



'''<summary>
'''This is a test class for RangeFTest and is intended
'''to contain all RangeFTest Unit Tests
'''</summary>
<TestClass()> 
Public Class RangeFTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for Contains
    '''</summary>
    <TestMethod()> 
    Public Sub ContainsTestMethod()
        Dim model As RangeF = Nothing ' TO_DO: Initialize to an appropriate value
        Dim target As RangeF = New RangeF(model) ' TO_DO: Initialize to an appropriate value
        Dim point As Single = 0.0! ' TO_DO: Initialize to an appropriate value
        Dim expected As Boolean = False ' TO_DO: Initialize to an appropriate value
        Dim actual As Boolean
        actual = target.Contains(point)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub
End Class
