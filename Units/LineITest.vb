﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports isr.Core.Types



'''<summary>
'''This is a test class for LineITest and is intended
'''to contain all LineITest Unit Tests
'''</summary>
<TestClass()> 
Public Class LineITest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for SetLine
    '''</summary>
    <TestMethod()> 
    Public Sub SetLineTestMethod()
        Dim target As LineI = New LineI() ' TO_DO: Initialize to an appropriate value
        Dim slope As Double = 0.0! ' TO_DO: Initialize to an appropriate value
        Dim offset As Double = 0.0! ' TO_DO: Initialize to an appropriate value
        target.SetLine(slope, offset)
        Assert.Inconclusive("A method that does not return a value cannot be verified.")
    End Sub

    ''' <summary>Tests the instantiation of the class and settings of the
    '''   instance values.</summary>
    <TestMethod()> 
    Public Sub IsEqual()

        Dim lineOne As New LineF(0, 0, 1, 1)
        Dim lineTwo As New LineF(0, 0, 1, 1.04)
        Dim offsetDelta As Double = Math.Abs(lineTwo.Offset - lineOne.Offset)
        Dim offsetMax As Double = Math.Max(Single.Epsilon, 0.1 * Math.Abs(lineOne.Insertion.Y - lineOne.Origin.Y))
        Dim slopeDelta As Double = Math.Abs(lineTwo.Slope - lineOne.Slope)
        Dim slopeMax As Double = Math.Max(Single.Epsilon, 0.1 * Math.Abs(lineTwo.Slope))
        If False Then
            Windows.Forms.MessageBox.Show(String.Format("offset diff: {0}", offsetDelta))
            Windows.Forms.MessageBox.Show(String.Format("offset lt: {0}", offsetMax))
            Windows.Forms.MessageBox.Show(String.Format("slope diff: {0}", slopeDelta))
            Windows.Forms.MessageBox.Show(String.Format("slope lt: {0}", slopeMax))
            Windows.Forms.MessageBox.Show(String.Format("outcome: {0}", offsetDelta < offsetMax AndAlso slopeDelta < slopeMax))
        End If
        Assert.IsTrue(lineOne.Equals(lineTwo, 0.1), "Comparing lines with 0.1 tolerance")

    End Sub

End Class
