﻿Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports isr.Core.Types



'''<summary>
'''This is a test class for RangeTest and is intended
'''to contain all RangeTest Unit Tests
'''</summary>
<TestClass()> 
Public Class RangeTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for Contains
    '''</summary>
    Public Sub ContainsTestHelper(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable})()
        Dim model As Range(Of T) = Nothing ' TO_DO: Initialize to an appropriate value
        Dim target As IRange(Of T) = New Range(Of T)(model) ' TO_DO: Initialize to an appropriate value
        Dim value As T = CType(Nothing, T) ' TO_DO: Initialize to an appropriate value
        Dim expected As Boolean = False ' TO_DO: Initialize to an appropriate value
        Dim actual As Boolean
        actual = target.Contains(value)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub

    <TestMethod()> 
    Public Sub ContainsTestMethod()
        Assert.Inconclusive("No appropriate type parameter is found to satisfies the type constraint(s) of T. " & 
                "Please call ContainsTestHelper(Of T) with appropriate type parameters.")
    End Sub

#Region " CUSTOM TESTS "

    Private testValue1 As Int32
    Private testValue2 As Int32

    ''' <summary>
    ''' Tests instantiating a generic range class.
    ''' </summary>
    <TestMethod()> 
    Public Sub RangeDouble()

        Dim rangeR As Range(Of Double)
        Dim min As Double = 0
        Dim max As Double = 1
        Dim mid As Double = 0.5 * (max + min)
        Dim delta As Double = 0.001
        rangeR = New Range(Of Double)(min, max)
        Dim message As String = "Actual range minimum '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, rangeR.Min, min)
        Assert.AreEqual(min, rangeR.Min, delta, message)
        message = "Actual range maximum '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, rangeR.Max, max)
        Assert.AreEqual(max, rangeR.Max, delta, message)
        message = "Range {0} does not contain '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, rangeR, mid)
        Assert.IsTrue(rangeR.Contains(mid), message)

    End Sub

    ''' <summary>
    ''' Tests instantiating a generic range class.
    ''' </summary>
    <TestMethod()> 
    Public Sub RangeInteger()

        Dim rangeI As Range(Of Int32)
        Dim min As Int32 = 0
        Dim max As Int32 = 10
        Dim mid As Int32 = (max + min) \ 2
        rangeI = New Range(Of Int32)(min, max)
        Dim message As String = "Actual range minimum '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, rangeI.Min, min)
        Assert.AreEqual(min, rangeI.Min, message)
        message = "Actual range maximum '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, rangeI.Max, max)
        Assert.AreEqual(max, rangeI.Max, message)
        message = "Range {0} does not contain '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, rangeI, mid)
        Assert.IsTrue(rangeI.Contains(mid), message)

    End Sub

    ''' <summary>
    ''' Tests instantiating a generic range class.
    ''' </summary>
    <TestMethod()> 
    Public Sub RangeExDouble()

        Dim rangeR As Calculable.Range(Of Double, Calculable.CalculatorR)
        Dim min As Double = 0
        Dim max As Double = 1
        Dim mid As Double = 0.5 * (max + min)
        Dim span As Double = max - min
        Dim delta As Double = 0.001
        Dim showMe As Boolean = False
        Dim message As String
        rangeR = Calculable.Range(Of Double, Calculable.CalculatorR).Empty
        message = "Actual zero range minimum '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, rangeR.Min, min)
        showMe = False
        Debug.WriteLineIf(showMe, message)
        Assert.AreEqual(min, rangeR.Min, delta, message)

        rangeR = Calculable.Range(Of Double, Calculable.CalculatorR).Unity
        message = "Actual unity range minimum '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, rangeR.Min, min)
        showMe = False
        Debug.WriteLineIf(showMe, message)
        Assert.AreEqual(min, rangeR.Min, delta, message)

        message = "Actual unity range maximum '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, rangeR.Max, max)
        showMe = True
        Debug.WriteLineIf(showMe, message)
        Assert.AreEqual(max, rangeR.Max, delta, message)

        message = "Actual unity range maximum norm '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, rangeR.MaximumNorm, max)
        showMe = False
        Debug.WriteLineIf(showMe, message)
        Assert.AreEqual(max, rangeR.MaximumNorm, delta, message)

        message = "Actual unity range maximum '{0}' not within extended range '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message,
            max + 0.01, Calculable.Range(Of Double, Calculable.CalculatorR).Extend(rangeR, 0.1))
        showMe = True
        Debug.WriteLineIf(showMe, message)
        Assert.IsTrue(rangeR.Contains(max + 0.01, 0.1), message)

        showMe = False
        rangeR = New Calculable.Range(Of Double, Calculable.CalculatorR)(min, max)
        message = "Actual range minimum '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, rangeR.Min, min)
        Debug.WriteLineIf(showMe, message)
        Assert.AreEqual(min, rangeR.Min, delta, message)
        message = "Actual range maximum '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, rangeR.Max, max)
        Debug.WriteLineIf(showMe, message)
        Assert.AreEqual(max, rangeR.Max, delta, message)
        message = "Range {0} does not contain '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, rangeR, mid)
        Debug.WriteLineIf(showMe, message)
        Assert.IsTrue(rangeR.Contains(mid), message)
        message = "Actual mid-range '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, rangeR.Mid, mid)
        Debug.WriteLineIf(showMe, message)
        Assert.AreEqual(mid, rangeR.Mid, delta, message)
        message = "Actual span '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, rangeR.Span, span)
        Debug.WriteLineIf(showMe, message)
        Assert.AreEqual(span, rangeR.Span, delta, message)

    End Sub

#End Region


End Class
