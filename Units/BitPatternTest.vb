﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports isr.Core.Types



'''<summary>
'''This is a test class for BitPatternTest and is intended
'''to contain all BitPatternTest Unit Tests
'''</summary>
<TestClass()> 
Public Class BitPatternTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    <ClassCleanup()> 
    Public Shared Sub MyClassCleanup()
    End Sub
    '
    'Use TestInitialize to run code before running each test
    <TestInitialize()> 
    Public Sub MyTestInitialize()
            ' set the two test values
        testValue1 = 2
        testValue2 = 3
    End Sub
    '
    'Use TestCleanup to run code after each test has run
    <TestCleanup()> 
    Public Sub MyTestCleanup()
        testValue1 = Nothing
        testValue2 = Nothing
    End Sub
    '
#End Region

    Private _instanceName As String = "BitPatternUnitTester"
    ''' <summary>Gets or sets the class instance name.</summary>
    ''' <value><c>InstanceName</c> is a String property that can be read from (read only).</value>
    ''' <remarks>Use this property to get this class instance name.</remarks>
    Public ReadOnly Property InstanceName() As String
        Get
            Return _instanceName
        End Get
    End Property

    ''' <summary>Gets or sets the status message.</summary>
    ''' <value><c>StatusMessage</c> is a String property.</value>
    ''' <remarks>Use this property to get or set the status message.</remarks>
    Public Property StatusMessage() As String

    Private testValue1 As Int32
    Private testValue2 As Int32

    '''<summary>
    '''A test for WasToggled
    '''</summary>
    <TestMethod()>
    Public Sub WasToggledTestMethod()
        Dim mask As Byte = 0 ' TO_DO: Initialize to an appropriate value
        Dim commonBitPattern As BitPattern = Nothing ' TO_DO: Initialize to an appropriate value
        Dim target As BitPattern = New BitPattern(mask, commonBitPattern) ' TO_DO: Initialize to an appropriate value
        Dim actual As Boolean
        actual = target.WasToggled
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub

    ''' <summary>tests the instantiation of the class and settings of the
    '''   instance values.</summary>
    <TestMethod()>
    Public Sub InstantiateAndTerminate()
        Dim mask As Byte = &HF
        Dim positiveLogic As Boolean = True
        Me.TestInstantiateAndTerminate(mask, positiveLogic)
    End Sub

    ''' <summary>tests instantiating a negative mask.</summary>
    <TestMethod(), ExpectedException(GetType(isr.Core.Types.BaseException))>
    Public Sub InstantiateNegativeMask()
        ' Dim bitPattern As isr.Core.Types.BitPattern
        Dim mask As Int64 = Int64.MinValue
        Dim positiveLogic As Boolean = True
        Me.TestInstantiateAndTerminate(mask, positiveLogic)
    End Sub

    ''' <summary>tests positive logic byte bitwise set and clear.</summary>
    <TestMethod()>
    Public Sub PositiveLogicSetClearByte()
        Dim mask As Byte = &H53
        Dim positiveLogic As Boolean = True
        Me.TestSetClear(mask, positiveLogic)
    End Sub

    ''' <summary>tests negative logic byte bitwise set and clear.</summary>
    <TestMethod()>
    Public Sub NegativeLogicSetClearByte()
        Dim mask As Byte = &H53
        Dim positiveLogic As Boolean = False
        Me.TestSetClear(mask, positiveLogic)
    End Sub

    ''' <summary>tests positive logic Int32 bitwise set and clear.</summary>
    <TestMethod()>
    Public Sub PositiveLogicSetClearInt32()
        Dim mask As Int32 = &H7FFF5001
        Dim positiveLogic As Boolean = True
        Me.TestSetClear(mask, positiveLogic)
    End Sub

    ''' <summary>tests negative logic Int32 bitwise set and clear.</summary>
    <TestMethod()>
    Public Sub NegativeLogicSetClearInt32()
        Dim mask As Int32 = &H7FFF5001
        Dim positiveLogic As Boolean = False
        Me.TestSetClear(mask, positiveLogic)
    End Sub

    ''' <summary>tests positive logic Int64 bitwise set and clear.</summary>
    <TestMethod()>
    Public Sub PositiveLogicSetClearInt64()
        Dim mask As Int64 = &H7FFF500112341234
        Dim positiveLogic As Boolean = True
        Me.TestSetClear(mask, positiveLogic)
    End Sub

    ''' <summary>tests negative logic Int64 bitwise set and clear.</summary>
    <TestMethod()>
    Public Sub NegativeLogicSetClearInt64()
        Dim mask As Int64 = &H7FFF500112341234
        Dim positiveLogic As Boolean = False
        Me.TestSetClear(mask, positiveLogic)
    End Sub

    ''' <summary>tests positive logic Int16 bitwise set and clear.</summary>
    <TestMethod()>
    Public Sub PositiveLogicSetClearInt16()
        Dim mask As Int16 = &H7001
        Dim positiveLogic As Boolean = True
        Me.TestSetClear(mask, positiveLogic)
    End Sub

    ''' <summary>tests negative logic Int16 bitwise set and clear.</summary>
    <TestMethod()>
    Public Sub NegativeLogicSetClearInt16()
        Dim mask As Int16 = &H7001
        Dim positiveLogic As Boolean = False
        Me.TestSetClear(mask, positiveLogic)
    End Sub

    ''' <summary>tests positive logic bitwise set and clear of elements.</summary>
    <TestMethod()>
    Public Sub PositiveLogicElements()
        Dim positiveLogic As Boolean = True
        TestElements(positiveLogic)
    End Sub

    ''' <summary>tests negative logic bitwise set and clear of elements.</summary>
    <TestMethod()>
    Public Sub NegativeLogicElements()
        Dim positiveLogic As Boolean = False
        TestElements(positiveLogic)
    End Sub

    ''' <summary>tests on off operations.</summary>
    <TestMethod()>
    Public Sub OnOff()
        Dim positiveLogic As Boolean = False
        Dim mask As Int64 = 2
        TestOnOff(mask, positiveLogic)
    End Sub

    ''' <summary>tests the instantiation of the class and settings of the
    '''   instance values.</summary>
    ''' <param name="mask">A Byte expression that specifies the mask.</param>
    ''' <param name="positiveLogic">A Boolean expression that is true for positive
    '''   and false for negative logic.</param>
    Private Sub TestInstantiateAndTerminate(ByVal mask As Byte, ByVal positiveLogic As Boolean)
        Dim bitPattern As isr.Core.Types.BitPattern
        bitPattern = New isr.Core.Types.BitPattern(mask, positiveLogic)
        Assert.AreEqual(Math.Floor(mask), bitPattern.Mask.ToString("X"), "Mask")  ' was fix
        Assert.AreEqual(positiveLogic, bitPattern.PositiveLogic, "Logic")
        bitPattern.Dispose()
        bitPattern = Nothing
    End Sub

    ''' <summary>tests the instantiation of the class and settings of the
    '''   instance values for a Int64 mask.</summary>
    ''' <param name="mask">A Int64 expression that specifies the mask.</param>
    ''' <param name="positiveLogic">A Boolean expression that is true for positive
    '''   and false for negative logic.</param>
    Private Sub TestInstantiateAndTerminate(ByVal mask As Int64, ByVal positiveLogic As Boolean)
        Dim bitPattern As isr.Core.Types.BitPattern
        bitPattern = New isr.Core.Types.BitPattern(mask, positiveLogic)
        Assert.AreEqual(mask.ToString("X"), bitPattern.Mask.ToString("X"), "Mask")
        Assert.AreEqual(positiveLogic, bitPattern.PositiveLogic, "Logic")
        bitPattern.Dispose()
        bitPattern = Nothing
    End Sub

    ''' <summary>tests byte bitwise set and clear.</summary>
    ''' <param name="mask">A Byte expression that specifies the mask.</param>
    ''' <param name="positiveLogic">A Boolean expression that is true for positive
    '''   and false for negative logic.</param>
    Private Sub TestSetClear(ByVal mask As Byte, ByVal positiveLogic As Boolean)
        Dim bitPattern As isr.Core.Types.BitPattern
        bitPattern = New isr.Core.Types.BitPattern(mask, positiveLogic)
        Dim sizeMask As Int64 = &H7FFFFFFFFFFFFFFF
        Assert.AreEqual(sizeMask.ToString("X"), bitPattern.SizeMask.ToString("X"), "Size Mask")
        bitPattern.BitwiseSet()
        Assert.AreEqual(True, bitPattern.IsOn, "Is On")
        Assert.AreEqual(False, bitPattern.IsOff, "Not Is Off")
        bitPattern.BitwiseClear()
        Assert.AreEqual(True, bitPattern.IsOff, "Is Off")
        Assert.AreEqual(False, bitPattern.IsOn, "Not Is On")
        bitPattern.Dispose()
        bitPattern = Nothing
    End Sub

    ''' <summary>tests Int32 bitwise set and clear.</summary>
    ''' <param name="mask">is an Int32 expression that specifies the mask.</param>
    ''' <param name="positiveLogic">A Boolean expression that is true for positive
    '''   and false for negative logic.</param>
    Private Sub TestSetClear(ByVal mask As Int32, ByVal positiveLogic As Boolean)
        Dim bitPattern As isr.Core.Types.BitPattern
        bitPattern = New isr.Core.Types.BitPattern(mask, positiveLogic)
        Dim sizeMask As Int64 = &H7FFFFFFFFFFFFFFF
        Assert.AreEqual(sizeMask.ToString("X"), bitPattern.SizeMask.ToString("X"), "Size Mask")
        bitPattern.BitwiseSet()
        Assert.AreEqual(True, bitPattern.IsOn, "Is On")
        Assert.AreEqual(False, bitPattern.IsOff, "Not Is Off")
        bitPattern.BitwiseClear()
        Assert.AreEqual(True, bitPattern.IsOff, "Is Off")
        Assert.AreEqual(False, bitPattern.IsOn, "Not Is On")
        bitPattern.Dispose()
        bitPattern = Nothing
    End Sub

    ''' <summary>tests Int64 bitwise set and clear.</summary>
    ''' <param name="mask">A Int64 expression that specifies the mask.</param>
    ''' <param name="positiveLogic">A Boolean expression that is true for positive
    '''   and false for negative logic.</param>
    Private Sub TestSetClear(ByVal mask As Int64, ByVal positiveLogic As Boolean)
        Dim bitPattern As isr.Core.Types.BitPattern
        bitPattern = New isr.Core.Types.BitPattern(mask, positiveLogic)
        Dim sizeMask As Int64 = &H7FFFFFFFFFFFFFFF
        Assert.AreEqual(sizeMask.ToString("X"), bitPattern.SizeMask.ToString("X"), "Size Mask")
        bitPattern.BitwiseSet()
        Assert.AreEqual(True, bitPattern.IsOn, "Is On")
        Assert.AreEqual(False, bitPattern.IsOff, "Not Is Off")
        bitPattern.BitwiseClear()
        Assert.AreEqual(True, bitPattern.IsOff, "Is Off")
        Assert.AreEqual(False, bitPattern.IsOn, "Not Is On")
        bitPattern.Dispose()
        bitPattern = Nothing
    End Sub

    ''' <summary>tests Int16 bitwise set and clear.</summary>
    ''' <param name="mask">A Int16 expression that specifies the mask.</param>
    ''' <param name="positiveLogic">A Boolean expression that is true for positive
    '''   and false for negative logic.</param>
    Private Sub TestSetClear(ByVal mask As Int16, ByVal positiveLogic As Boolean)
        Dim bitPattern As isr.Core.Types.BitPattern
        bitPattern = New isr.Core.Types.BitPattern(mask, positiveLogic)
        Dim sizeMask As Int64 = &H7FFFFFFFFFFFFFFF
        Assert.AreEqual(sizeMask.ToString("X"), bitPattern.SizeMask.ToString("X"), "Size Mask")
        bitPattern.BitwiseSet()
        Assert.AreEqual(True, bitPattern.IsOn, "Is On")
        Assert.AreEqual(False, bitPattern.IsOff, "Not Is Off")
        bitPattern.BitwiseClear()
        Assert.AreEqual(True, bitPattern.IsOff, "Is Off")
        Assert.AreEqual(False, bitPattern.IsOn, "Not Is On")
        bitPattern.Dispose()
        bitPattern = Nothing
    End Sub

    ''' <summary>tests bit wise operations using a main bit pattern
    '''   with member bit patterns asserting the same data.</summary>
    ''' <param name="positiveLogic">A Boolean expression that is true for positive
    '''   and false for negative logic.</param>
    Private Sub TestElements(ByVal positiveLogic As Boolean)
        Dim mainBitPattern As isr.Core.Types.BitPattern
        Dim mask As Int16 = &H7FFF
        mainBitPattern = New isr.Core.Types.BitPattern(mask, positiveLogic)
        Dim bitOne As isr.Core.Types.BitPattern = New isr.Core.Types.BitPattern(&H1, mainBitPattern)
        Dim bitTwo As isr.Core.Types.BitPattern = New isr.Core.Types.BitPattern(&H2, mainBitPattern)
        Dim bitThree As isr.Core.Types.BitPattern = New isr.Core.Types.BitPattern(&H3, mainBitPattern)
        bitOne.BitwiseSet()
        Assert.AreEqual(True, bitOne.IsOn, "Bit one is On")
        bitTwo.BitwiseSet()
        Assert.AreEqual(True, bitTwo.IsOn, "Bit two is On")
        Assert.AreEqual(True, bitThree.IsOn, "Bit three is On")
        bitThree.BitwiseClear()
        Assert.AreEqual(True, bitOne.IsOff, "Bit one is Off")
        Assert.AreEqual(True, bitTwo.IsOff, "Bit two is Off")
        Assert.AreEqual(True, bitThree.IsOff, "Bit three is Off")
        bitTwo.BitwiseToggle()
        Assert.AreEqual(True, bitOne.IsOff, "Bit one is Off")
        Assert.AreEqual(True, bitTwo.IsOn, "Bit two is on")
        Assert.AreEqual(False, bitOne.WasToggled, "Bit one was not toggled")
        Assert.AreEqual(True, bitTwo.WasToggled, "Bit two was toggled")
        bitOne.Dispose()
        bitOne = Nothing
        bitTwo.Dispose()
        bitTwo = Nothing
        bitThree.Dispose()
        bitThree = Nothing
        mainBitPattern.Dispose()
        mainBitPattern = Nothing
    End Sub

    ''' <summary>tests bit on off and high low operations.</summary>
    ''' <param name="mask">A Int64 expression that specifies the mask.</param>
    ''' <param name="positiveLogic">A Boolean expression that is true for positive
    '''   and false for negative logic.</param>
    Private Sub TestOnOff(ByVal mask As Int64, ByVal positiveLogic As Boolean)
        Dim bitPattern As isr.Core.Types.BitPattern
        bitPattern = New isr.Core.Types.BitPattern(mask, positiveLogic)
        bitPattern.IsOn = True
        Assert.AreEqual(True, bitPattern.IsOn, "Bit is On")
        Assert.AreEqual(False, bitPattern.IsOff, "Bit is not off")
        bitPattern.IsOn = False
        Assert.AreEqual(False, bitPattern.IsOn, "Bit is not on")
        Assert.AreEqual(True, bitPattern.IsOff, "Bit is off")
        bitPattern.IsOff = False
        Assert.AreEqual(True, bitPattern.IsOn, "Bit is on")
        Assert.AreEqual(False, bitPattern.IsOff, "Bit is not off")
        bitPattern.IsOff = True
        Assert.AreEqual(True, bitPattern.IsOff, "Bit is Off")
        Assert.AreEqual(False, bitPattern.IsOn, "Bit is not on")
        bitPattern.IsHigh = True
        Assert.AreEqual(True, bitPattern.IsHigh, "Bit is high")
        Assert.AreEqual(False, bitPattern.IsLow, "Bit is not low")
        bitPattern.IsLow = True
        Assert.AreEqual(False, bitPattern.IsHigh, "Bit is not high")
        Assert.AreEqual(True, bitPattern.IsLow, "Bit is low")
        bitPattern.Dispose()
        bitPattern = Nothing
    End Sub

End Class

