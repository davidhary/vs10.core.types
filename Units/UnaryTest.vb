﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports isr.Core.Types



'''<summary>
'''This is a test class for UnaryTest and is intended
'''to contain all UnaryTest Unit Tests
'''</summary>
<TestClass()> 
Public Class UnaryTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for Ceiling
    '''</summary>
    <TestMethod()> 
    Public Sub CeilingTestMethod()
        Dim value As Double = 0.0! ' TO_DO: Initialize to an appropriate value
        Dim relativeDecimal As Integer = 0 ' TO_DO: Initialize to an appropriate value
        Dim expected As Double = 0.0! ' TO_DO: Initialize to an appropriate value
        Dim actual As Double
        actual = Unary.Ceiling(value, relativeDecimal)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub

    ''' <summary>tests unary ceiling operations.</summary>
    <TestMethod()> 
    Public Sub UnaryCeiling()
        Dim value As Double = 1.12345678
        value = 1.12345678
        Assert.AreEqual(1.13, isr.Core.Types.Unary.Ceiling(value, 2), "Ceiling +1.1234x, 2")
        value = -1.12345678
        Assert.AreEqual(-1.12, isr.Core.Types.Unary.Ceiling(value, 2), "Ceiling -1.1234x, 2")
    End Sub

    ''' <summary>tests unary decade operations.</summary>
    <TestMethod()> 
    Public Sub UnaryExponent()
        Dim value As Double = 1.12345678
        value = 1.23
        Assert.AreEqual(0, isr.Core.Types.Unary.Exponent(value), String.Format(Globalization.CultureInfo.CurrentCulture, 
              "Exponent {0}", value))
        value = 12.34
        Assert.AreEqual(1, isr.Core.Types.Unary.Exponent(value), String.Format(Globalization.CultureInfo.CurrentCulture, 
              "Exponent {0}", value))
        value = -1.23
        Assert.AreEqual(0, isr.Core.Types.Unary.Exponent(value), String.Format(Globalization.CultureInfo.CurrentCulture, 
              "Exponent {0}", value))
        value = 0.1234
        Assert.AreEqual(-1, isr.Core.Types.Unary.Exponent(value), String.Format(Globalization.CultureInfo.CurrentCulture, 
              "Exponent {0}", value))
    End Sub

    ''' <summary>tests unary decade operations.</summary>
    ''' <example>
    ''' Unary.Exponent(11.12) returns 0.<p>
    ''' Unary.Decade(111.23) returns 0.</p><p>
    ''' Unary.Exponent(1111.123) returns 3.</p><p>
    ''' Unary.Exponent(-11.12) returns 0.</p><p>
    ''' Unary.Decade(-111.23) returns 1.</p><p>
    ''' Unary.Exponent(-1111.123) returns 3.</p><p>
    ''' Unary.Exponent(0.1234) returns 0</p><p>
    ''' Unary.Exponent(0.01234) returns 0</p><p>
    ''' Unary.Exponent(0.0001234) returns -3</p><p>
    ''' Unary.Exponent(-0.1234) returns 0</p><p>
    ''' Unary.Exponent(-0.01234) returns -3</p><p>
    ''' Unary.Exponent(-0.0001234) returns -3</p>
    ''' </example>
    <TestMethod()> 
    Public Sub UnaryEngineeringxponent()
        Dim value As Double = 1.12345678
        value = 1.23
        Assert.AreEqual(0, isr.Core.Types.Unary.Exponent(value, True), String.Format(Globalization.CultureInfo.CurrentCulture, 
              "Eng. Exponent {0}", value))
        value = 12.34
        Assert.AreEqual(0, isr.Core.Types.Unary.Exponent(value, True), String.Format(Globalization.CultureInfo.CurrentCulture, 
              "Eng. Exponent {0}", value))
        value = 1234.5
        Assert.AreEqual(3, isr.Core.Types.Unary.Exponent(value, True), String.Format(Globalization.CultureInfo.CurrentCulture, 
              "Eng. Exponent {0}", value))
        value = -1.23
        Assert.AreEqual(0, isr.Core.Types.Unary.Exponent(value, True), String.Format(Globalization.CultureInfo.CurrentCulture, 
              "Eng. Exponent {0}", value))
        value = -1234.5
        Assert.AreEqual(3, isr.Core.Types.Unary.Exponent(value, True), String.Format(Globalization.CultureInfo.CurrentCulture, 
              "Eng. Exponent {0}", value))
        value = 0.123
        Assert.AreEqual(0, isr.Core.Types.Unary.Exponent(value, True), String.Format(Globalization.CultureInfo.CurrentCulture, 
              "Eng. Exponent {0}", value))
        value = 0.0123
        Assert.AreEqual(0, isr.Core.Types.Unary.Exponent(value, True), String.Format(Globalization.CultureInfo.CurrentCulture, 
              "Eng. Exponent {0}", value))
        value = 0.00123
        Assert.AreEqual(-3, isr.Core.Types.Unary.Exponent(value, True), String.Format(Globalization.CultureInfo.CurrentCulture, 
              "Eng. Exponent {0}", value))
        value = 0.000123
        Assert.AreEqual(-3, isr.Core.Types.Unary.Exponent(value, True), String.Format(Globalization.CultureInfo.CurrentCulture, 
              "Eng. Exponent {0}", value))
    End Sub

    ''' <summary>tests unary fixed format operations.</summary>
    <TestMethod()> 
    Public Sub UnaryFixedFormat()
        Dim value As Double = 1.12345678
        value = 1.123
        Assert.AreEqual("Fixed Format +1.123, 1", "0.0", isr.Core.Types.Unary.FixedFormat(value, 1))
        value = 11.23
        Assert.AreEqual("Decade +11.23, 1", "0", isr.Core.Types.Unary.FixedFormat(value, 1))
        value = -1.123
        Assert.AreEqual("Decade -1.123, 1", "0.0", isr.Core.Types.Unary.FixedFormat(value, 1))
        value = 0.1234
        Assert.AreEqual("Decade 0.1234, 1", "0.00", isr.Core.Types.Unary.FixedFormat(value, 1))
    End Sub

    ''' <summary>tests unary floor operations.</summary>
    <TestMethod()> 
    Public Sub UnaryFloor()
        Dim value As Double = 1.12345678
        value = 1.123
        Assert.AreEqual(1.1, isr.Core.Types.Unary.Floor(value, 1), "Floor +1.123x, 1")
        value = -1.123
        Assert.AreEqual(-1.2, isr.Core.Types.Unary.Floor(value, 1), "Floor -1.123x, 1")
        value = 11.123
        Assert.AreEqual(11, isr.Core.Types.Unary.Floor(value, 1), "Floor 11.123x, 1")
        value = 0.123
        Assert.AreEqual(0.12, isr.Core.Types.Unary.Floor(value, 1), "Floor 0.123x, 1")
        value = 11.123
        Assert.AreEqual(10, isr.Core.Types.Unary.Floor(value, 0), "Floor 11.123x, 0")
    End Sub

    ''' <summary>tests unary decimal value operations.</summary>
    <TestMethod()> 
    Public Sub UnaryDecimalValue()
        Dim value As Double = 1.12345678
        value = 1.123
        Assert.AreEqual(10, isr.Core.Types.Unary.DecimalValue(value, 1), "DecimalValue +1.123x, 1")
        value = -1.123
        Assert.AreEqual(10, isr.Core.Types.Unary.DecimalValue(value, 1), "DecimalValue -1.123x, 1")
        value = 11.123
        Assert.AreEqual(100, isr.Core.Types.Unary.DecimalValue(value, 1), "DecimalValue 11.123x, 1")
        value = 0.123
        Assert.AreEqual(1, isr.Core.Types.Unary.DecimalValue(value, 1), "DecimalValue 0.123x, 1")
        value = 11.123
        Assert.AreEqual(10, isr.Core.Types.Unary.DecimalValue(value, 0), "DecimalValue 11.123x, 0")
    End Sub

End Class
