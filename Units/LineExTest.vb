﻿Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports isr.Core.Types



'''<summary>
'''This is a test class for LineExTest and is intended
'''to contain all LineExTest Unit Tests
'''</summary>
<TestClass()> 
Public Class LineExTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for UpdateSlopeOffset
    '''</summary>
    Public Sub UpdateSlopeOffsetTestHelper(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable}, TC As {ICalculator(Of T), New})()
        Dim target As LineEx_Accessor(Of T, TC) = New LineEx_Accessor(Of T, TC)() ' TO_DO: Initialize to an appropriate value
        target.UpdateSlopeOffset()
        Assert.Inconclusive("A method that does not return a value cannot be verified.")
    End Sub

    <TestMethod(), 
     DeploymentItem("isr.Core.Types.dll")> 
    Public Sub UpdateSlopeOffsetTestMethod()
        Assert.Inconclusive("No appropriate type parameter is found to satisfies the type constraint(s) of T. " & 
                "Please call UpdateSlopeOffsetTestHelper(Of T, TC) with appropriate type paramete" & 
                "rs.")
    End Sub

#Region " CUSTOM TESTS "

    Private testValue1 As Int32
    Private testValue2 As Int32

    ''' <summary>
    ''' Tests instantiating a generic line class.
    ''' </summary>
    <TestMethod()> 
    Public Sub LineDouble()

        Dim lineR As Line(Of Double)
        Dim x1 As Double = 0
        Dim y1 As Double = 0
        Dim x2 As Double = 1
        Dim y2 As Double = 1
        Dim offset As Double = 0
        Dim delta As Double = 0.001
        lineR = New Line(Of Double)(x1, y1, x2, y2)
        Dim message As String
        Dim showMe As Boolean = False

        message = "Actual line x1 '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, lineR.X1, x1)
        Assert.AreEqual(x1, lineR.X1, delta, message)

        message = "Actual line y1 '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, lineR.Y1, y1)
        Assert.AreEqual(y1, lineR.Y1, delta, message)

        Assert.IsFalse(lineR.Changed, "Line tagged as not changed")

    End Sub

    ''' <summary>
    ''' Tests instantiating a generic line class.
    ''' </summary>
    <TestMethod()> 
    Public Sub LineInteger()

        Dim lineI As Line(Of Int32)
        Dim x1 As Int32 = 0
        Dim y1 As Int32 = 0
        Dim x2 As Int32 = 10
        Dim y2 As Int32 = 10
        lineI = New Line(Of Int32)(x1, y1, x2, y2)
        Dim message As String

        message = "Actual line x1 '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, lineI.X1, x1)
        Assert.AreEqual(x1, lineI.X1, message)

        message = "Actual line y1 '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, lineI.Y1, y1)
        Assert.AreEqual(y1, lineI.Y1, message)

        Assert.IsFalse(lineI.Changed, "Linetagged as not changed")

    End Sub

    ''' <summary>
    ''' Tests instantiating a generic line class.
    ''' </summary>
    <TestMethod()> 
    Public Sub LineExDouble()

        Dim lineR As LineEx(Of Double, CalculatorR)
        Dim x1 As Double = 0
        Dim y1 As Double = 0
        Dim x2 As Double = 1
        Dim y2 As Double = 1
        Dim delta As Double = 0.001
        Dim showMe As Boolean = False
        Dim message As String

        lineR = LineEx(Of Double, CalculatorR).Zero

        message = "Actual zero line x1 '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, lineR.X1, x1)
        showMe = False
        Debug.WriteLineIf(showMe, "testing: " & message)
        Assert.AreEqual(x1, lineR.X1, delta, message)

        lineR = LineEx(Of Double, CalculatorR).Unity

        message = "Actual unity line x2 '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, lineR.X2, x2)
        showMe = False
        Debug.WriteLineIf(showMe, "testing: " & message)
        Assert.AreEqual(x2, lineR.X2, delta, message)

        message = "Actual unity line y1 '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, lineR.Y1, y1)
        showMe = False
        Debug.WriteLineIf(showMe, "testing: " & message)
        Assert.AreEqual(y1, lineR.Y1, delta, message)

        message = "Actual unity line maximum norm '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, lineR.MaximumNorm, y2)
        showMe = False
        Debug.WriteLineIf(showMe, "testing: " & message)
        Assert.AreEqual(y2, lineR.MaximumNorm, delta, message)

        lineR = New LineEx(Of Double, CalculatorR)(x1, y1, x2, y2)

        message = "Actual line x1 '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, lineR.X1, x1)
        showMe = False
        Debug.WriteLineIf(showMe, "testing: " & message)
        Assert.AreEqual(x1, lineR.X1, delta, message)

        message = "Actual line y2 '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, lineR.Y2, y2)
        showMe = False
        Debug.WriteLineIf(showMe, "testing: " & message)
        Assert.AreEqual(y2, lineR.Y2, delta, message)

        Assert.IsFalse(lineR.Changed, "Line tagged as not changed")

        message = "Actual line slope '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, lineR.Slope, y2)
        showMe = False
        Debug.WriteLineIf(showMe, "testing: " & message)
        Assert.AreEqual(y2, lineR.Slope, delta, message)

        message = "Actual line offset '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, lineR.Offset, 0)
        showMe = False
        Debug.WriteLineIf(showMe, "testing: " & message)
        Assert.AreEqual(0, lineR.Offset, delta, message)

        Dim offsetLine As New LineEx(Of Double, CalculatorR)(lineR)
        Dim slope As Double = 1
        Dim offset As Double = 0.01
        delta = 0.001
        offsetLine.Y1 += offset
        offsetLine.Y2 += offset

        message = "Actual line '{0}' slope '{1}' not equal expected value '{2}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, offsetLine, offsetLine.Slope, slope)
        showMe = False
        Debug.WriteLineIf(showMe, "testing: " & message)
        Assert.AreEqual(slope, offsetLine.Slope, delta, message)

        message = "Actual line '{0}' offset '{1}' not equal expected value '{2}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, offsetLine, offsetLine.Offset, offset)
        showMe = False
        Debug.WriteLineIf(showMe, "testing: " & message)
        Assert.AreEqual(offset, offsetLine.Offset, delta, message)

        slope = 0.99
        offsetLine.Y2 -= offset
        message = "Actual line slope '{0}' not equal expected value '{1}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, offsetLine.Slope, slope)
        showMe = False
        Debug.WriteLineIf(showMe, "testing: " & message)
        Assert.AreEqual(slope, offsetLine.Slope, delta, message)

        Dim tolerance As Double = 0.1
        message = "Line one '{0}' not equal line 2 '{1}' with tolerance '{2}'"
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, lineR, offsetLine, tolerance)
        showMe = False
        Debug.WriteLineIf(showMe, "testing: " & message)
        Assert.IsTrue(lineR.Equals(offsetLine, tolerance), message)

    End Sub

#End Region

End Class
