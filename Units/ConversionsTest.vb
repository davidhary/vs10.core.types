﻿Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports isr.Core.Types
Imports System.Windows.Forms



'''<summary>
'''This is a test class for ConversionsTest and is intended
'''to contain all ConversionsTest Unit Tests
'''</summary>
<TestClass()> 
Public Class ConversionsTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for ConvertToString
    '''</summary>
    <TestMethod()> 
    Public Sub ConvertToStringTestMethod()
        Dim dataValue As Object = Nothing ' TO_DO: Initialize to an appropriate value
        Dim dataFormat As String = String.Empty ' TO_DO: Initialize to an appropriate value
        Dim dataType As Type = Nothing ' TO_DO: Initialize to an appropriate value
        Dim expected As String = String.Empty ' TO_DO: Initialize to an appropriate value
        Dim actual As String
        actual = Conversions.ConvertToString(dataValue, dataFormat, dataType)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub

#Region " DUMMY  TESTS "

    ''' <summary>Tests the addition of two test values.</summary>
    ''' <remarks>Use this method to test the addition of two test values.
    '''   To fail this test, set expected result value to 6.</remarks>
    <TestMethod()> 
    Public Sub DummyAdd()
        Const expectedResult As Int32 = 5
        Dim result As Double
        result = testValue1 + testValue2
        Assert.AreEqual(expectedResult, result)
    End Sub

    ''' <summary>Tests for overflow exception.</summary>
    ''' <remarks>Use this method to test division by zero for overflow exception.
    '''   To fail this test, set expected exception to System.OverflowException.</remarks>
    <TestMethod(), ExpectedException(GetType(System.OverflowException))> 
    Public Sub DummyDivideByZero()
        Dim zero As Int32 = 0
        Dim result As Int32 = 8
        result = Convert.ToInt32(result / zero, Application.CurrentCulture)
    End Sub

    ''' <summary>Tests equality of various types.</summary>
    ''' <remarks>Use this method to test equality with reference to specific quantity 
    '''   names. To fail this test, set dblAlmostActual to 11.45.</remarks>
    <TestMethod()> 
    Public Sub DummyEquals()
        Const intExpected As Int32 = 12
        Const intActual As Int32 = 12
        Const dblActual As Double = 12
        Const dblAlmostActual As Double = 11.95
        Const dblTolerance As Double = 0.5

        Assert.AreEqual(intExpected, intActual)
        Assert.AreEqual(Convert.ToInt64(intExpected), Convert.ToInt64(dblActual))
        Assert.AreEqual(intExpected, intActual, "Size")
        Assert.AreEqual(intExpected, dblAlmostActual, dblTolerance, "Capacity")

    End Sub

    ''' <summary>Tests for a specific exception.</summary>
    ''' <remarks>Use this method to test for a specific exception to be raised. 
    '''   names. To fail this tests, set the expected exception to System.InvalidCastException.</remarks>
    <TestMethod(), ExpectedException(GetType(System.InvalidCastException))> 
    Public Sub DummyException()
        Throw New InvalidCastException
    End Sub

    ''' <summary>Tests ignoring a test.</summary>
    ''' <remarks>Use this method to test if the TestPanel ignore this test.
    '''   To fail this tests, remove the ignore attribute.</remarks>
    <TestMethod(), Ignore()> 
    Public Sub DummyIgnored()
        ' does not matter what we type the test is not run
        Throw New InvalidExpressionException
    End Sub

#End Region

#Region " PRIVATE  and  PROTECTED "

    ''' <summary>Tests the instantiation of the class and settings of the
    '''   instance values.</summary>
    Private Sub testDeserializeSerialize(ByVal defaultValue As Object, ByVal input As String, 
      ByVal expected As Object)
        Dim actual As Object = isr.Core.Types.Conversions.Deserialize(input, defaultValue)
        Dim title As String = String.Format(Globalization.CultureInfo.CurrentCulture, "Deserialize from {0}={1} to {2}={3}", 
          input.GetType.ToString, input, actual.GetType.ToString, actual)
        Assert.AreEqual(expected, actual, title)
        actual = isr.Core.Types.Conversions.Serialize(actual)
        title = String.Format(Globalization.CultureInfo.CurrentCulture, "Serialize from {0}={1} to {2}={3}", 
            actual.GetType.ToString, actual, input.GetType.ToString, input)
        Assert.AreEqual(input, actual, title)
    End Sub

    ''' <summary>Tests the instantiation of the class and settings of the
    '''   instance values.</summary>
    Private Sub testDeserializeSerializeByType(ByVal defaultType As String, ByVal input As String, 
      ByVal expected As Object)
        Dim actual As Object = isr.Core.Types.Conversions.Deserialize(input, System.Type.GetType(defaultType))
        Dim title As String = String.Format(Globalization.CultureInfo.CurrentCulture, "Deserialize {0} from {1}={2} to {3}={4}", 
          defaultType, input.GetType.ToString, input, actual.GetType.ToString, actual)
        Assert.AreEqual(expected, actual, title)
        actual = isr.Core.Types.Conversions.Serialize(actual)
        title = String.Format(Globalization.CultureInfo.CurrentCulture, "Serialize from {0}={1} to {2}={3}", 
            actual.GetType.ToString, actual, input.GetType.ToString, input)
        Assert.AreEqual(input, actual, title)
    End Sub

    ''' <summary>Tests the instantiation of the class and settings of the
    '''   instance values.</summary>
    ''' <param name="mask">A Byte expression that specifies the mask.</param>
    ''' <param name="positiveLogic">A Boolean expression that is true for positive
    '''   and false for negative logic.</param>
    Private Sub testInstantiateAndTerminate(ByVal mask As Byte, ByVal positiveLogic As Boolean)
        '    Dim _bitPattern As isr.Core.Types.BitPattern
        '    Me._bitPattern = New isr.Core.Types.BitPattern(mask, positiveLogic)
        '    Assert.AreEqual( Hex(mask), Hex(Me._bitPattern.Mask),"Mask")
        '    Assert.AreEqual( positiveLogic, Me._bitPattern.PositiveLogic,"Logic")
        '    Me._bitPattern.Dispose()
        '    Me._bitPattern = Nothing
    End Sub

#End Region


#Region " CUSTOM TESTS "

    Private testValue1 As Int32
    Private testValue2 As Int32

    ''' <summary>Tests the instantiation of the class and settings of the
    '''   instance values.</summary>
    <TestMethod()> 
    Public Sub InstantiateAndTerminate()
        Dim mask As Byte = &HF
        Dim positiveLogic As Boolean = True
        Me.testInstantiateAndTerminate(mask, positiveLogic)
    End Sub

    ''' <summary>tests deserialize Int32.</summary>
    <TestMethod()> 
    Public Sub DeserializeInt32ByType()
        Dim defaultType As String = "System.Int32"
        Dim value As String = "10"
        Dim expected As Int32 = 10
        testDeserializeSerializeByType(defaultType, value, expected)
        '    Dim actual As Int32 = CType(Deserialize(value, System.Type.GetType(defaultType)), Int32)
        '    ' Dim outcome2 as Int32 = Deserialize(value,System.Type.GetType("System.Int32"))
        '   Assert.AreEqual(String.Format(Globalization.CultureInfo.CurrentCulture, "Deserialize Int32 {0}", value), expected, actual)
        '  Assert.AreEqual(String.Format(Globalization.CultureInfo.CurrentCulture, "Serialize Int32 {0}", actual), value, Serialize(actual))
    End Sub

    ''' <summary>tests deserialize byte by type.</summary>
    <TestMethod()> 
    Public Sub DeserializeByteByType()
        Dim defaultType As String = "System.Byte"
        Dim value As String = "10"
        Dim expected As Byte = 10
        testDeserializeSerializeByType(defaultType, value, expected)
    End Sub

    ''' <summary>tests deserialize Boolean by type.</summary>
    <TestMethod()> 
    Public Sub DeserializeBooleanByType()
        Dim defaultType As String = "System.Boolean"
        Dim value As String = "True"
        Dim expected As Boolean = True
        testDeserializeSerializeByType(defaultType, value, expected)
    End Sub

    ''' <summary>tests deserialize Boolean by type.</summary>
    <TestMethod()> 
    Public Sub DeserializeDateTimeByType()
        Dim defaultType As String = "System.DateTime"
        Dim expected As DateTime = Date.Now
        Dim value As String = expected.Ticks.ToString
        testDeserializeSerializeByType(defaultType, value, expected)
    End Sub

    ''' <summary>tests deserialize Decimal by type.</summary>
    <TestMethod()> 
    Public Sub DeserializeDecimalByType()
        Dim defaultType As String = "System.Decimal"
        Dim expected As Decimal = 10.11D
        Dim value As String = "10.11"
        testDeserializeSerializeByType(defaultType, value, expected)
    End Sub

    ''' <summary>tests deserialize String by type.</summary>
    <TestMethod()> 
    Public Sub DeserializeStringByType()
        Dim defaultType As String = "System.String"
        Dim expected As String = "A String"
        Dim value As String = "A String"
        testDeserializeSerializeByType(defaultType, value, expected)
    End Sub

    ''' <summary>tests deserialize Int32.</summary>
    <TestMethod()> 
    Public Sub DeserializeString()
        Dim defaultValue As String = String.Empty
        Dim expected As String = "A String"
        Dim value As String = "A String"
        testDeserializeSerialize(defaultValue, value, expected)
    End Sub

    ''' <summary>tests deserialize Int32.</summary>
    <TestMethod()> 
    Public Sub DeserializeDecimal()
        Dim defaultValue As Decimal = 0
        Dim value As String = "10.11"
        Dim expected As Decimal = 10.11D
        testDeserializeSerialize(defaultValue, value, expected)
    End Sub

    ''' <summary>tests deserialize Int32.</summary>
    <TestMethod()> 
    Public Sub DeserializeInt32()
        Dim defaultValue As Int32 = 0
        Dim value As String = "10"
        Dim expected As Int32 = 10
        testDeserializeSerialize(defaultValue, value, expected)
    End Sub

    ''' <summary>tests deserialize byte.</summary>
    <TestMethod()> 
    Public Sub DeserializeByte()
        Dim defaultValue As Byte = 0
        Dim value As String = "10"
        Dim expected As Byte = 10
        testDeserializeSerialize(defaultValue, value, expected)
    End Sub

    ''' <summary>tests deserialize boolean.</summary>
    <TestMethod()> 
    Public Sub DeserializeBoolean()
        Dim defaultValue As Boolean = False
        Dim value As String = "True"
        Dim expected As Boolean = True
        testDeserializeSerialize(defaultValue, value, expected)
    End Sub

#End Region


End Class
