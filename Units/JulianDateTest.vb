﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports isr.Core.Types
Imports System.Windows.Forms



'''<summary>
'''This is a test class for JulianDateTest and is intended
'''to contain all JulianDateTest Unit Tests
'''</summary>
<TestClass()> 
Public Class JulianDateTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for AddMilliseconds
    '''</summary>
    <TestMethod()> 
    Public Sub AddMillisecondsTestMethod()
        Dim target As JulianDate = New JulianDate() ' TO_DO: Initialize to an appropriate value
        Dim milliseconds As Double = 0.0! ' TO_DO: Initialize to an appropriate value
        target.AddMilliseconds(milliseconds)
        Assert.Inconclusive("A method that does not return a value cannot be verified.")
    End Sub

#Region " DUMMY  TESTS "

    ''' <summary>tests the addition of two test values.</summary>
    ''' <remarks>Use this method to test the addition of two test values.
    '''   To fail this test, set expected result value to 6.</remarks>
    <TestMethod()> 
    Public Sub DummyAdd()
        Const expectedResult As Int32 = 5
        Dim result As Double
        result = testValue1 + testValue2
        Assert.AreEqual(expectedResult, result)
    End Sub

    ''' <summary>tests for overflow exception.</summary>
    ''' <remarks>Use this method to test division by zero for overflow exception.
    '''   To fail this test, set expected exception to System.OverflowException.</remarks>
    <TestMethod(), ExpectedException(GetType(System.OverflowException))> 
    Public Sub DummyDivideByZero()
        Dim zero As Int32 = 0
        Dim result As Int32 = 8
        result = Convert.ToInt32(result / zero, Application.CurrentCulture)
    End Sub

    ''' <summary>tests equality of various types.</summary>
    ''' <remarks>Use this method to test equality with reference to specific quantity 
    '''   names. To fail this test, set dblAlmostActual to 11.45.</remarks>
    <TestMethod()> 
    Public Sub DummyEquals()
        Const intExpected As Int32 = 12
        Const intActual As Int32 = 12
        Const dblActual As Double = 12
        Const dblAlmostActual As Double = 11.95
        Const dblTolerance As Double = 0.5

        Assert.AreEqual(intExpected, intActual)
        Assert.AreEqual(Convert.ToInt64(intExpected), Convert.ToInt64(dblActual))
        Assert.AreEqual(intExpected, intActual, "Size")
        Assert.AreEqual(intExpected, dblAlmostActual, dblTolerance, "Capacity")

    End Sub

    ''' <summary>tests for a specific exception.</summary>
    ''' <remarks>Use this method to test for a specific exception to be raised. 
    '''   names. To fail this tests, set the expected exception to System.InvalidCastException.</remarks>
    <TestMethod(), ExpectedException(GetType(System.InvalidCastException))> 
    Public Sub DummyException()
        Throw New InvalidCastException
    End Sub

    ''' <summary>tests ignoring a test.</summary>
    ''' <remarks>Use this method to test if the TestPanel ignore this test.
    '''   To fail this tests, remove the ignore attribute.</remarks>
    <TestMethod(), Ignore()> 
    Public Sub DummyIgnored()
        ' does not matter what we type the test is not run
        Throw New InvalidExpressionException
    End Sub

#End Region

#Region " CUSTOM TESTS "

    Private testValue1 As Int32
    Private testValue2 As Int32

    ''' <summary>tests the instantiation of the class and settings of the
    '''   instance values.</summary>
    <TestMethod()> 
    Public Sub InstantiateAndTerminate()
        '    Dim _bitPattern As isr.Core.Types.BitPattern
        '    Me._bitPattern = New isr.Core.Types.BitPattern(mask, positiveLogic)
        '    Assert.AreEqual( Hex(mask), Hex(Me._bitPattern.Mask), "Mask")
        '    Assert.AreEqual(positiveLogic, Me._bitPattern.PositiveLogic, "Logic")
        '    Me._bitPattern.Dispose()
        '    Me._bitPattern = Nothing
    End Sub

    ''' <summary>tests the instantiation of the class and settings of the
    '''   instance values.</summary>
    <TestMethod()> 
    Public Sub DateTimeConversions()
        Dim dateNow As DateTime = DateTime.Now
        Dim julianDay As Double = isr.Core.Types.JulianDate.FromDateTime(dateNow)
        Dim convertedDateTime As DateTime = isr.Core.Types.JulianDate.ToDateTime(julianDay)
        Assert.AreEqual(True, dateNow.Equals(convertedDateTime), "Converted time")
    End Sub

#End Region


End Class
