<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class TestPanel
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> 
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
      Try
        If disposing AndAlso components IsNot Nothing Then
          components.Dispose()
        End If
      Finally
        MyBase.Dispose(disposing)
      End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> 
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Me.aboutButton = New System.Windows.Forms.Button
    Me.notesMessageList = New isr.Controls.MessagesBox
    Me.testButton = New System.Windows.Forms.Button
    Me.statusLabel = New System.Windows.Forms.Label
    Me.exitButton = New System.Windows.Forms.Button
    Me.cancelButton1 = New System.Windows.Forms.Button
    Me.SuspendLayout()
    '
    'aboutButton
    '
    Me.aboutButton.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.aboutButton.Location = New System.Drawing.Point(158, 240)
    Me.aboutButton.Name = "aboutButton"
    Me.aboutButton.Size = New System.Drawing.Size(60, 23)
    Me.aboutButton.TabIndex = 10
    Me.aboutButton.Text = "&About"
    '
    'notesMessageList
    '
    Me.notesMessageList.Bullet = "* "
    Me.notesMessageList.Location = New System.Drawing.Point(14, 48)
    Me.notesMessageList.Multiline = True
    Me.notesMessageList.Name = "notesMessageList"
    Me.notesMessageList.PresetCount = 50
    Me.notesMessageList.ReadOnly = True
    Me.notesMessageList.ResetCount = 100
    Me.notesMessageList.ScrollBars = System.Windows.Forms.ScrollBars.Both
    Me.notesMessageList.Size = New System.Drawing.Size(280, 176)
    Me.notesMessageList.TabIndex = 9
    Me.notesMessageList.TimeFormat = "HH:mm:ss. "
    Me.notesMessageList.UsingBullet = True
    Me.notesMessageList.UsingTimeBullet = True
    '
    'testButton
    '
    Me.testButton.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.testButton.Location = New System.Drawing.Point(86, 240)
    Me.testButton.Name = "testButton"
    Me.testButton.Size = New System.Drawing.Size(60, 23)
    Me.testButton.TabIndex = 8
    Me.testButton.Text = "&Test"
    '
    'statusLabel
    '
    Me.statusLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.statusLabel.Location = New System.Drawing.Point(6, 8)
    Me.statusLabel.Name = "statusLabel"
    Me.statusLabel.Size = New System.Drawing.Size(288, 32)
    Me.statusLabel.TabIndex = 6
    Me.statusLabel.Text = "Status: Ready"
    '
    'exitButton
    '
    Me.exitButton.DialogResult = System.Windows.Forms.DialogResult.OK
    Me.exitButton.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.exitButton.Location = New System.Drawing.Point(230, 240)
    Me.exitButton.Name = "exitButton"
    Me.exitButton.Size = New System.Drawing.Size(60, 23)
    Me.exitButton.TabIndex = 5
    Me.exitButton.Text = "E&xit"
    '
    'cancelButton1
    '
    Me.cancelButton1.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.cancelButton1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.cancelButton1.Location = New System.Drawing.Point(14, 240)
    Me.cancelButton1.Name = "cancelButton1"
    Me.cancelButton1.Size = New System.Drawing.Size(60, 23)
    Me.cancelButton1.TabIndex = 7
    Me.cancelButton1.Text = "&Cancel"
    '
    'Form1
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(302, 275)
    Me.Controls.Add(Me.aboutButton)
    Me.Controls.Add(Me.notesMessageList)
    Me.Controls.Add(Me.testButton)
    Me.Controls.Add(Me.statusLabel)
    Me.Controls.Add(Me.exitButton)
    Me.Controls.Add(Me.cancelButton1)
    Me.Name = "Form1"
    Me.Text = "Form1"
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Private WithEvents aboutButton As System.Windows.Forms.Button
  Private WithEvents notesMessageList As isr.Controls.MessagesBox
  Private WithEvents testButton As System.Windows.Forms.Button
  Private WithEvents statusLabel As System.Windows.Forms.Label
  Private WithEvents exitButton As System.Windows.Forms.Button
  Private WithEvents cancelButton1 As System.Windows.Forms.Button
End Class
