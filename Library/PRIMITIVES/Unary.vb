''' <summary>Defines the basic Unary type.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="03/19/04" by="David" revision="1.0.1539.x">
''' Created
''' </history>
Public NotInheritable Class Unary

    ''' <summary>Prevents instantiation of this type, which defines only static members.</summary>
    Private Sub New()
    End Sub

#Region " SHARED "

    ''' <summary>Returns a value that is rounded up to the specified significant digit.</summary>
    ''' <param name="value">value which to use in the calculation.</param>
    ''' <param name="relativeDecimal">The decimal digit relative to the most significant digit 
    '''   to use.</param>
    ''' <returns>A rounded up value to the specified relative digit</returns>
    ''' <example>
    '''   Unary.Ceiling(1.123456,2) returns 1.13.<p>
    '''   Unary.Ceiling(-1.123456,2) returns -1.12.</p>
    ''' </example>
    Shared Function Ceiling(ByVal value As Double, ByVal relativeDecimal As Integer) As Double

        Dim decimalDigitValue As Double

        ' check if the value is zero
        If value = 0 Then

            ' if so, return This value
            Return value

        Else

            ' get the representation of the significant digit
            decimalDigitValue = DecimalValue(value, -relativeDecimal)

            ' get the new value
            Return decimalDigitValue * System.Math.Ceiling(value / decimalDigitValue)

        End If

    End Function

    ''' <summary>Returns the decimal value of a number.</summary>
    ''' <param name="value">Value which to use in the calculation</param>
    ''' <param name="decimalLeftShift">Decimal power by which to shift the digit.</param>
    ''' <returns>The decimal value of the value shifted by the specified decimalPlaces.</returns>
    ''' <example>
    '''   Unary.DecimalValue(1.123,1) returns 10.<p>
    '''   Unary.DecimalValue(11.23,1) returns 100.</p><p>
    '''   Unary.DecimalValue(-1.123,1) returns 10.</p><p>
    '''   Unary.DecimalValue(0.1234,1) returns 1.</p><p>
    '''   Unary.DecimalValue(11.12,0) returns 10.</p>
    ''' </example>
    Shared Function DecimalValue(ByVal value As Double, ByVal decimalLeftShift As Integer) As Double

        ' check if the value is zero
        If value = 0 Then

            ' if so, return This value
            Return 0

        Else

            ' get the representation of the most significant digit
            Return Math.Pow(10, Unary.Exponent(value) + decimalLeftShift)

        End If

    End Function

    ''' <summary>Returns the exponent.</summary>
    ''' <param name="value">A <see cref="System.Double">Double</see> value.</param>
    ''' <returns>The most significant decade of the value.</returns>
    ''' <example>
    '''   Unary.Exponent(1.12) returns 0.<p>
    '''   Unary.Decade(11.23) returns 1.</p><p>
    '''   Unary.Exponent(-1.123) returns 0.</p><p>
    '''   Unary.Exponent(0.1234) returns -1</p>
    ''' </example>
    ''' <seealso cref="Decimal"/>
    Shared Function Exponent(ByVal value As Double) As Integer

        ' check if the value is zero
        If value = 0 Then

            ' if so, return This value
            Return 0

        Else

            ' get the exponent based on the most significant digit.
            '     Return Convert.ToInt32(System.Math.Floor(System.Math.Log(System.Math.Abs(value))) / System.Math.Log(10.0#))
            Return Convert.ToInt32(System.Math.Floor(System.Math.Log10(System.Math.Abs(value))))
        End If

    End Function

    ''' <summary>Returns the exponent.</summary>
    ''' <param name="value">A <see cref="System.Double">Double</see> value.</param>
    ''' <returns>The most significant decade of the value.</returns>
    ''' <example>
    '''   Unary.Exponent(11.12) returns 0.<p>
    '''   Unary.Decade(111.23) returns 1.</p><p>
    '''   Unary.Exponent(1111.123) returns 3.</p><p>
    '''   Unary.Exponent(-11.12) returns 0.</p><p>
    '''   Unary.Decade(-111.23) returns 1.</p><p>
    '''   Unary.Exponent(-1111.123) returns 3.</p><p>
    '''   Unary.Exponent(0.1234) returns 0</p><p>
    '''   Unary.Exponent(0.01234) returns 0</p><p>
    '''   Unary.Exponent(0.0001234) returns -3</p><p>
    '''   Unary.Exponent(-0.1234) returns 0</p><p>
    '''   Unary.Exponent(-0.01234) returns 0</p><p>
    '''   Unary.Exponent(-0.001234) returns -3</p><p>
    '''   Unary.Exponent(-0.0001234) returns -3</p>
    ''' </example>
    ''' <seealso cref="DecimalValue"/>
    Shared Function Exponent(ByVal value As Double, ByVal useEngineeringScale As Boolean) As Integer

        ' check if the value is zero
        If value = 0 Then

            ' if so, return This value
            Return 0

        Else

            If useEngineeringScale Then
                ' Use a power of 10 that is a multiple of 3 (engineering scale)
                Return 3 * (Exponent(value) \ 3)
            Else
                Return Exponent(value)
            End If

        End If

    End Function

    ''' <summary>Returns a suggested fixed format based requested most
    '''  significant decimalPlaces.</summary>
    ''' <param name="value">Number to format</param>
    ''' <param name="decimalPlaces">The number of decimal places beyond the most
    '''     significant digit.</param>
    ''' <returns>A fix format string</returns>
    ''' <example>
    '''   Unary.FixedFormat(1.123,1) returns 0.0.<p>
    '''   Unary.FixedFormat(11.23,1) returns 0.</p><p>
    '''   Unary.FixedFormat(-1.123,1) returns 0.0.</p><p>
    '''   Unary.FixedFormat(0.1234,1) returns 0.0.</p>
    ''' </example>
    Shared Function FixedFormat(ByVal value As Double, ByVal decimalPlaces As Integer) As String

        decimalPlaces = decimalPlaces - Unary.Exponent(value)
        If decimalPlaces > 0 Then
            Return "0." & New String("0"c, decimalPlaces)
        Else
            Return "0"
        End If

    End Function

    ''' <summary>Returns a value that is rounded down to the specified significant digit.</summary>
    ''' <param name="value">value which to use in the calculation.</param>
    ''' <param name="relativeDecimal">The decimal digit relative to the most significant digit 
    '''   to use.</param>
    ''' <returns>A rounded down value to the specified relative digit</returns>
    ''' <example>
    '''   Unary.Floor(1.123,1) returns 1.1.<p>
    '''   Unary.Floor(11.23,1) returns 11.</p><p>
    '''   Unary.Floor(-1.123,1) returns -1.3.</p><p>
    '''   Unary.Floor(0.1234,1) returns 0.12.</p><p>
    '''   Unary.Floor(11.12,0) returns 10.</p>
    ''' </example>
    Shared Function Floor(ByVal value As Double, ByVal relativeDecimal As Integer) As Double

        Dim decimalDigitValue As Double

        ' check if the value is zero
        If value = 0 Then

            ' if so, return This value
            Return value

        Else

            ' get the representation of the significant digit
            decimalDigitValue = DecimalValue(value, -relativeDecimal)

            ' get the new value
            Return decimalDigitValue * System.Math.Floor(value / decimalDigitValue)

        End If

    End Function

#End Region

End Class

