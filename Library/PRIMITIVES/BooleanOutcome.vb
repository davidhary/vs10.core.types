''' <summary>
''' Include a Boolean result value extended with failure or success information.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="11/15/2007" by="David" revision="1.1.2875.x">
''' Created
''' </history>
Public Class BooleanOutcome

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BooleanOutcome" /> class.
    ''' </summary>
    ''' <param name="outcome">if set to <c>True</c> [outcome].</param>
    Public Sub New(ByVal outcome As Boolean)

        MyBase.New()

        Me._outcome = outcome

    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BooleanOutcome" /> class.
    ''' </summary>
    ''' <param name="outcome">if set to <c>True</c> [outcome].</param>
    ''' <param name="synopsis">The synopsis.</param>
    ''' <param name="details">The details.</param>
    ''' <param name="hint">The hint.</param>
    Public Sub New(ByVal outcome As Boolean, ByVal synopsis As String, ByVal details As String, ByVal hint As String)

        Me.New(outcome)

        Me._Details = details
        Me._Synopsis = synopsis
        Me._Hint = hint

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Gets or sets the outcome details message.
    ''' </summary>
    Public Property Details() As String

    ''' <summary>
    ''' Gets or sets the outcome hint message.
    ''' </summary>
    Public Property Hint() As String

    Private _outcome As Boolean
    ''' <summary>
    ''' Gets the outcome. 
    ''' </summary>
    Public ReadOnly Property Outcome() As Boolean
        Get
            Return Me._outcome
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the outcome synopsis message.
    ''' </summary>
    Public Property Synopsis() As String

#End Region

End Class
