''' <summary> Defines a <see cref="System.TimeSpan">TimeSpan</see> range class. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="04/30/04" by="David" revision="1.0.1581.x"> Create. </history>
Public Class RangeTimeSpan

#Region " SHARED "

    ''' <summary> Gets the empty range. </summary>
    ''' <value> A <see cref="System.TimeSpan.MinValue"/> value with
    ''' <see cref="System.TimeSpan.MaxValue"/>
    ''' minimum value and <see cref="System.TimeSpan.MinValue"/> for the maximum value. </value>
    Public Shared ReadOnly Property [Empty]() As RangeTimeSpan
        Get
            Return New RangeTimeSpan(TimeSpan.MaxValue, TimeSpan.MinValue)
        End Get
    End Property

    ''' <summary> Return the range of the specified data array. </summary>
    ''' <param name="values"> The data array. </param>
    ''' <returns> The calculated range. </returns>
    Public Shared Function GetRange(ByVal values() As TimeSpan) As RangeTimeSpan

        ' return the unit range if no data
        If values Is Nothing Then
            Return RangeTimeSpan.Unity
        End If

        ' initialize the range values to the first value
        Dim temp As TimeSpan
        temp = values(0)
        Dim min As TimeSpan = temp
        Dim max As TimeSpan = temp

        ' Loop over each point in the arrays
        For i As Integer = 0 To values.Length - 1
            temp = values(i)
            If temp.CompareTo(min) < 0 Then
                min = temp
            ElseIf temp.CompareTo(max) > 0 Then
                max = temp
            End If
        Next i

        Return New RangeTimeSpan(min, max)

    End Function

    ''' <summary> Gets the Unity range. </summary>
    ''' <value> A <see cref="RangeTimeSpan"/> [0,1] value. </value>
    Public Shared ReadOnly Property Unity() As RangeTimeSpan
        Get
            Return New RangeTimeSpan(TimeSpan.Zero, TimeSpan.FromSeconds(1))
        End Get
    End Property

    ''' <summary> Gets the zero range value. </summary>
    ''' <value> A <see cref="RangeTimeSpan"/> value. </value>
    Public Shared ReadOnly Property Zero() As RangeTimeSpan
        Get
            Return New RangeTimeSpan(TimeSpan.Zero, TimeSpan.Zero)
        End Get
    End Property

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Constructs a <see cref="RangeTimeSpan"/> instance by its limits. </summary>
    ''' <param name="minValue"> A <see cref="System.TimeSpan">TimeSpan</see> expression that
    ''' specifics the minimum range. </param>
    ''' <param name="maxValue"> A <see cref="System.TimeSpan">TimeSpan</see> expression that
    ''' specifics the maximum range. </param>
    Public Sub New(ByVal minValue As TimeSpan, ByVal maxValue As TimeSpan)

        MyBase.new()
        SetRange(minValue, maxValue)

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <param name="model"> The RangeTimeSpan object from which to copy. </param>
    Public Sub New(ByVal model As RangeTimeSpan)

        MyBase.New()
        If model IsNot Nothing Then
            SetRange(model._Min, model._Max)
        End If

    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> = casting operator. </summary>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As RangeTimeSpan, ByVal right As RangeTimeSpan) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return RangeTimeSpan.Equals(left, right)
        End If
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As RangeTimeSpan, ByVal right As RangeTimeSpan) As Boolean
        Return Not RangeTimeSpan.Equals(left, right)
    End Operator

    ''' <summary> Returns True if equal. </summary>
    ''' <remarks> Range Time Spans are the same if the have the same
    ''' <see cref="Type">min</see> and <see cref="Type">max</see> values. </remarks>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> <c>True</c> if equals. </returns>
    Public Overloads Shared Function Equals(ByVal left As RangeTimeSpan, ByVal right As RangeTimeSpan) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return left.Max.Equals(right.Max) AndAlso left.Min.Equals(right.Min)
        End If
    End Function

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    ''' <see cref="T:System.Object" />. </param>
    ''' <returns> true if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, false. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso (Object.ReferenceEquals(Me, obj) OrElse RangeTimeSpan.Equals(Me, CType(obj, RangeTimeSpan)))
    End Function

    ''' <summary> Returns True if the value of the <param>compared</param> equals to the instance
    ''' value. </summary>
    ''' <remarks> Range Time Spans are the same if the have the same
    ''' <see cref="Type">min</see> and <see cref="Type">max</see> values. </remarks>
    ''' <param name="compared"> The <see cref="RangeTimeSpan">Range Time Span</see> to compare for
    ''' equality with this instance. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal compared As RangeTimeSpan) As Boolean
        If compared Is Nothing Then
            Return False
        Else
            Return RangeTimeSpan.Equals(Me, compared)
        End If
    End Function

#End Region

#Region " METHODS "

    ''' <summary> Returns true if the point value is within the range. </summary>
    ''' <param name="point"> A <see cref="System.Date">Date</see> point value&gt; </param>
    ''' <returns> True if value above or equal to minimum or below or equal to maximum. </returns>
    Public Function Contains(ByVal point As Date) As Boolean

        Return point.CompareTo(Me.Min) >= 0 AndAlso point.CompareTo(Me.Max) <= 0

    End Function

    ''' <summary> Returns true if the point value is within the range. </summary>
    ''' <param name="point">     A <see cref="System.Date">Date</see> point value&gt; </param>
    ''' <param name="tolerance"> Tolerance for comparison. </param>
    ''' <returns> True if value above or equal to minimum - tolerance or below or equal to maximum +
    ''' tolerance. </returns>
    Public Function Contains(ByVal point As TimeSpan, ByVal tolerance As TimeSpan) As Boolean

        Return (point.Subtract(Me.Min.Subtract(tolerance)).Ticks >= 0) AndAlso
               Me.Max.Subtract(point.Subtract(tolerance)).Ticks >= 0

    End Function

    ''' <summary> Extend this range to include both its present values and the specified range. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="range"> A <see cref="RangeTimeSpan"/> value. </param>
    ''' <returns> Extended range. </returns>
    Public Function ExtendRange(ByVal range As RangeTimeSpan) As RangeTimeSpan

        If range Is Nothing Then
            Throw New ArgumentNullException("range")
        End If

        If Me.Min.CompareTo(range.Min) > 0 Then
            Me.Min = range.Min
        End If

        If Me.Max.CompareTo(range.Max) < 0 Then
            Me.Max = range.Max
        End If

        Return Me

    End Function

    ''' <summary> Creates a unique hash code. </summary>
    ''' <returns> An <see cref="System.integer">integer</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Integer
        Return Me.Min.GetHashCode Xor Me.Max.GetHashCode
    End Function

    ''' <summary> Sets the range based on the extrema. </summary>
    ''' <remarks> Use this class to set the range. </remarks>
    ''' <param name="minValue"> A <see cref="System.TimeSpan">TimeSpan</see> expression that
    ''' specifies the minimum value of the range. </param>
    ''' <param name="maxValue"> A <see cref="System.TimeSpan">TimeSpan</see> expression that
    ''' specifies the maximum value of the range. </param>
    Public Overloads Sub SetRange(ByVal minValue As TimeSpan, ByVal maxValue As TimeSpan)

        Me.Min = minValue
        Me.Max = maxValue

    End Sub

    ''' <summary> Returns the default string representation of the range. </summary>
    ''' <returns> A String        Return String.Format(Globalization.CultureInfo.CurrentCulture,
    ''' "({0},{1})",                             Me.Min.ToString(), Me.Max.ToString())    End
    ''' Function#End Region#Region " PROPERTIES "    ''' <summary> Gets the maximum value of the
    ''' range. </summary> that represents this object. </returns>
    Public Overrides Function ToString() As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture,
                             "({0},{1})",
                             Me.Min.ToString(), Me.Max.ToString())
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets the maximum value of the range. </summary>
    ''' <value> A <see cref="System.TimeSpan">TimeSpan</see> property. </value>
    Public Property Max() As TimeSpan

    ''' <summary> Gets the minimum value of the range. </summary>
    ''' <value> A <see cref="System.TimeSpan">TimeSpan</see> property. </value>
    Public Property Min() As TimeSpan

    ''' <summary> Gets the range value of the range. </summary>
    ''' <value> A <see cref="System.TimeSpan"/> property. </value>
    Public ReadOnly Property Range() As TimeSpan
        Get
            Return Me.Max.Subtract(Me.Min)
        End Get
    End Property

#End Region

End Class
