''' <summary>Handles bit pattern storage and masking.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/27/04" by="David" revision="1.0.1518.x">
''' Created
''' </history>
Public Class BitPattern

    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Instantiates a Byte bit pattern.</summary>
    ''' <param name="mask">A Byte expression that specifies mask.</param>
    ''' <param name="PositiveLogic">A Boolean expression that specifies how
    '''   a bit pattern is interpreted as On or Off.  With positive logic, an on value
    '''   equals the bit pattern mask.</param>
    Public Sub New(ByVal mask As Byte, ByVal positiveLogic As Boolean)

        ' instantiate the base class
        MyBase.New()
        onInstantiate(mask, positiveLogic)

    End Sub

    ''' <summary>Instantiates an Int32 bit pattern.</summary>
    ''' <param name="mask">A Int32 expression that specifies mask.</param>
    ''' <param name="PositiveLogic">A Boolean expression that specifies how
    '''   a bit pattern is interpreted as On or Off.  With positive logic, an on value
    '''   equals the bit pattern mask.</param>
    Public Sub New(ByVal mask As Int32, ByVal positiveLogic As Boolean)

        ' instantiate the base class
        MyBase.New()
        onInstantiate(mask, positiveLogic)

    End Sub

    ''' <summary>Instantiates a Int64 bit pattern.</summary>
    ''' <param name="mask">A Int64 expression that specifies mask.</param>
    ''' <param name="PositiveLogic">A Boolean expression that specifies how
    '''   a bit pattern is interpreted as On or Off.  With positive logic, an on value
    '''   equals the bit pattern mask.</param>
    Public Sub New(ByVal mask As Int64, ByVal positiveLogic As Boolean)

        ' instantiate the base class
        MyBase.New()
        onInstantiate(mask, positiveLogic)

    End Sub

    ''' <summary>Instantiates a Int16 bit pattern.</summary>
    ''' <param name="mask">A Int16 expression that specifies mask.</param>
    ''' <param name="PositiveLogic">A Boolean expression that specifies how
    '''   a bit pattern is interpreted as On or Off.  With positive logic, an on value
    '''   equals the bit pattern mask.</param>
    Public Sub New(ByVal mask As Int16, ByVal positiveLogic As Boolean)

        ' instantiate the base class
        MyBase.New()
        onInstantiate(mask, positiveLogic)

    End Sub

    ''' <summary>Instantiates a byte bit pattern with reference to a common bit pattern 
    '''   element.</summary>
    ''' <param name="mask">A Byte expression that specifies mask.</param>
    ''' <param name="commonBitPattern">A <see cref="BitPattern"/>BitPattern expression
    '''   that specifies the bit pattern which all member bit patterns use.</param>
    ''' <remarks>Use this constructor to force this bit pattern to reference 
    '''   the value of the common bit pattern.  This was, a set of bit patterns can
    '''   use their own masks sharing a single common bit pattern element.</remarks>
    Public Sub New(ByVal mask As Byte, ByVal commonBitPattern As BitPattern)

        If commonBitPattern Is Nothing Then

            onInstantiate(mask, True)

        Else
            ' instantiate the base class
            onInstantiate(mask, commonBitPattern.PositiveLogic)

            ' set the reference to the bit pattern element of the common bit pattern
            BitPatternElement = commonBitPattern.BitPatternElement

        End If

    End Sub

    ''' <summary>Instantiates a Int32 bit pattern with reference to a common bit pattern 
    '''   element.</summary>
    ''' <param name="mask">A Int32 expression that specifies mask.</param>
    ''' <param name="commonBitPattern">A <see cref="BitPattern"/>BitPattern expression
    '''   that specifies the bit pattern which all member bit patterns use.</param>
    ''' <remarks>Use this constructor to force this bit pattern to reference 
    '''   the value of the common bit pattern.  This was, a set of bit patterns can
    '''   use their own masks sharing a single common bit pattern element.</remarks>
    Public Sub New(ByVal mask As Int32, ByVal commonBitPattern As BitPattern)

        If commonBitPattern Is Nothing Then

            onInstantiate(mask, True)

        Else
            ' instantiate the base class
            onInstantiate(mask, commonBitPattern.PositiveLogic)

            ' set the reference to the bit pattern element of the common bit pattern
            BitPatternElement = commonBitPattern.BitPatternElement

        End If

    End Sub

    ''' <summary>Instantiates a Int64 bit pattern with reference to a common bit pattern
    '''   element.</summary>
    ''' <param name="mask">A Int64 expression that specifies mask.</param>
    ''' <param name="commonBitPattern">A <see cref="BitPattern"/>BitPattern expression
    '''   that specifies the bit pattern which all member bit patterns use.</param>
    ''' <remarks>Use this constructor to force this bit pattern to reference 
    '''   the value of the common bit pattern.  This was, a set of bit patterns can
    '''   use their own masks sharing a single common bit pattern element.</remarks>
    Public Sub New(ByVal mask As Int64, ByVal commonBitPattern As BitPattern)

        If commonBitPattern Is Nothing Then

            onInstantiate(mask, True)

        Else
            ' instantiate the base class
            onInstantiate(mask, commonBitPattern.PositiveLogic)

            ' set the reference to the bit pattern element of the common bit pattern
            BitPatternElement = commonBitPattern.BitPatternElement

        End If

    End Sub

    ''' <summary>Instantiates a Int16 bit pattern with reference to a common bit pattern 
    '''   element.</summary>
    ''' <param name="mask">A Int16 expression that specifies mask.</param>
    ''' <param name="commonBitPattern">A <see cref="BitPattern"/>BitPattern expression
    '''   that specifies the bit pattern which all member bit patterns use.</param>
    ''' <remarks>Use this constructor to force this bit pattern to reference 
    '''   the value of the common bit pattern.  This was, a set of bit patterns can
    '''   use their own masks sharing a single common bit pattern element.</remarks>
    Public Sub New(ByVal mask As Int16, ByVal commonBitPattern As BitPattern)

        If commonBitPattern Is Nothing Then

            onInstantiate(mask, True)

        Else
            ' instantiate the base class
            onInstantiate(mask, commonBitPattern.PositiveLogic)

            ' set the reference to the bit pattern element of the common bit pattern
            BitPatternElement = commonBitPattern.BitPatternElement

        End If

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._bitPatternElement = Nothing

                End If

                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

            End Try

        End If

    End Sub

#End Region

#Region " SHARED "

    ''' <summary>Returns a bitwise clear bit pattern.</summary>
    ''' <param name="value">A Byte expression that specifies the value to be
    '''   bit clear.</param>
    ''' <param name="mask">A Byte expression that specifies the bits to clear.
    '''   Only the bits whose value is high are affected.</param>
    ''' <returns>A Byte data type</returns>
    Public Shared Function BitwiseClear(ByVal value As Byte, ByVal mask As Byte) As Byte

        Return value And (Not mask)

    End Function

    ''' <summary>Returns a bitwise clear bit pattern.</summary>
    ''' <param name="value">A Int32 expression that specifies the value to be
    '''   bit clear.</param>
    ''' <param name="mask">A Int32 expression that specifies the bits to clear.
    '''   Only the bits whose value is high are affected.</param>
    ''' <returns>A Int32 data type</returns>
    Public Shared Function BitwiseClear(ByVal value As Int32, ByVal mask As Int32) As Int32

        Return value And (Not mask)

    End Function

    ''' <summary>Returns a bitwise clear bit pattern.</summary>
    ''' <param name="value">A Int64 expression that specifies the value to be
    '''   bit clear.</param>
    ''' <param name="mask">A Int64 expression that specifies the bits to clear.
    '''   Only the bits whose value is high are affected.</param>
    ''' <returns>A Int64 data type</returns>
    Public Shared Function BitwiseClear(ByVal value As Int64, ByVal mask As Int64) As Int64

        Return value And (Not mask)

    End Function

    ''' <summary>Returns a bitwise clear bit pattern.</summary>
    ''' <param name="value">A Int16 expression that specifies the value to be
    '''   bit clear.</param>
    ''' <param name="mask">A Int16 expression that specifies the bits to clear.
    '''   Only the bits whose value is high are affected.</param>
    ''' <returns>A Int16 data type</returns>
    Public Shared Function BitwiseClear(ByVal value As Int16, ByVal mask As Int16) As Int16

        Return value And (Not mask)

    End Function

    ''' <summary>Returns a bitwise set bit pattern.</summary>
    ''' <param name="value">A Byte expression that specifies the value to be
    '''   bit set.</param>
    ''' <param name="mask">A Byte expression that specifies the bits to set.
    '''   Only the bits whose value is high are affected.</param>
    ''' <returns>A Byte data type</returns>
    Public Shared Function BitwiseSet(ByVal value As Byte, ByVal mask As Byte) As Byte

        Return value Or mask

    End Function

    ''' <summary>Returns a bitwise set bit pattern.</summary>
    ''' <param name="value">A Int32 expression that specifies the value to be
    '''   bit set.</param>
    ''' <param name="mask">A Int32 expression that specifies the bits to set.
    '''   Only the bits whose value is high are affected.</param>
    ''' <returns>A Int32 data type</returns>
    Public Shared Function BitwiseSet(ByVal value As Int32, ByVal mask As Int32) As Int32

        Return value Or mask

    End Function

    ''' <summary>Returns a bitwise set bit pattern.</summary>
    ''' <param name="value">A Int64 expression that specifies the value to be
    '''   bit set.</param>
    ''' <param name="mask">A Int64 expression that specifies the bits to set.
    '''   Only the bits whose value is high are affected.</param>
    ''' <returns>A Int64 data type</returns>
    Public Shared Function BitwiseSet(ByVal value As Int64, ByVal mask As Int64) As Int64

        Return value Or mask

    End Function

    ''' <summary>Returns a bitwise set bit pattern.</summary>
    ''' <param name="value">A Int16 expression that specifies the value to be
    '''   bit set.</param>
    ''' <param name="mask">A Int16 expression that specifies the bits to set.
    '''   Only the bits whose value is high are affected.</param>
    ''' <returns>A Int16 data type</returns>
    Public Shared Function BitwiseSet(ByVal value As Int16, ByVal mask As Int16) As Int16

        Return value Or mask

    End Function

#End Region

#Region " METHODS "

    ''' <summary>Bitwise clears the bit pattern.</summary>
    Public Sub BitwiseClear()

        If Me.PositiveLogic Then
            Me.BitPatternElement.Value = BitPattern.BitwiseClear(Me.BitPatternElement.Value, Me.Mask)
        Else
            Me.BitPatternElement.Value = BitPattern.BitwiseSet(Me.BitPatternElement.Value, Me.Mask)
        End If

    End Sub

    ''' <summary>Bitwise sets the bit pattern.</summary>
    Public Sub BitwiseSet()

        If Me.PositiveLogic Then
            Me.BitPatternElement.Value = BitPattern.BitwiseSet(Me.BitPatternElement.Value, Me.Mask)
        Else
            Me.BitPatternElement.Value = BitPattern.BitwiseClear(Me.BitPatternElement.Value, Me.Mask)
        End If

    End Sub

    ''' <summary>Bitwise toggles the bit pattern.</summary>
    Public Sub BitwiseToggle()

        Me.BitPatternElement.Value = (Me.BitPatternElement.Value Xor Me.Mask)

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>Gets or sets the bit pattern data.</summary>
    ''' <value><c>BitPatternElement</c> is a <see cref="BitPattern"/>BitPattern property.</value>
    ''' <remarks>Each bit pattern instantiates its own bit pattern element.  However, if
    '''   you wish a set of patterns to address the same data value, reference the 
    '''   bit pattern elements of all the patterns to one bit pattern.  All the patterns
    '''   will then have the same value source.
    ''' <example><code>
    '''   Sub onInstantiate()
    '''     Dim mainBitPattern as BitPattern = new BitPattern(255, True)
    '''     Dim bitOne as BitPattern = new BitPattern(1, mainBitPattern)
    '''     Dim bitTwo as BitPattern = new BitPattern(2, mainBitPattern)
    '''     Dim bitFour as BitPattern = new BitPattern(4, mainBitPattern)
    '''     bitFour.BitwiseSet
    '''     Debug.WriteLine(bitFour.Value.ToString)
    '''   End Sub
    ''' </code></example></remarks>
    Public Property BitPatternElement() As BitPatternElement

    ''' <summary>Gets or sets the High status of the bit pattern.</summary>
    ''' <value><c>IsHigh</c> is a Boolean property.</value>
    Public Property IsHigh() As Boolean
        Get
            If Me.PositiveLogic Then
                Return Me.IsOn
            Else
                Return Me.IsOff
            End If
        End Get
        Set(ByVal Value As Boolean)
            If Me.PositiveLogic Then
                Me.IsOn = Value
            Else
                Me.IsOff = Value
            End If
        End Set
    End Property

    ''' <summary>Gets or sets the Low status of the bit pattern.</summary>
    ''' <value><c>IsLow</c> is a Boolean property.</value>
    Public Property IsLow() As Boolean
        Get
            If Me.PositiveLogic Then
                Return Me.IsOff
            Else
                Return Me.IsOn
            End If
        End Get
        Set(ByVal Value As Boolean)
            If Me.PositiveLogic Then
                Me.IsOff = Value
            Else
                Me.IsOn = Value
            End If
        End Set
    End Property

    ''' <summary>Gets or sets the Off status of the bit pattern.</summary>
    ''' <value><c>IsOn</c> is a Boolean property.</value>
    Public Property IsOff() As Boolean
        Get
            Return (Me.BitPatternElement.Value And Me.Mask) = Me.OffValue
        End Get
        Set(ByVal Value As Boolean)
            If Value Then
                Me.BitwiseClear()
            Else
                Me.BitwiseSet()
            End If
        End Set
    End Property

    ''' <summary>Gets or sets the On status of the bit pattern.</summary>
    ''' <value><c>IsOn</c> is a Boolean property.</value>
    Public Property IsOn() As Boolean
        Get
            ' get the On status
            Return (Me.BitPatternElement.Value And Me.Mask) = Me.OnValue
        End Get
        Set(ByVal Value As Boolean)
            If Value Then
                Me.BitwiseSet()
            Else
                Me.BitwiseClear()
            End If
        End Set
    End Property

    Private _positiveLogic As Boolean = True
    ''' <summary>Gets or sets the logic of this bit pattern.</summary>
    ''' <value><c>PositiveLogic</c> is a Boolean property.</value>
    ''' <remarks>Positive logic means that the on value is the same as 
    '''   the mask value.  Negative logic means that the off value is 
    '''   the same as the mask value.</remarks>
    Public Property PositiveLogic() As Boolean
        Get
            Return Me._positiveLogic
        End Get
        Set(ByVal Value As Boolean)
            Me._positiveLogic = Value
            If Value Then
                Me._onValue = Me.Mask And Me.SizeMask
                Me._offValue = 0
            Else
                Me._onValue = 0
                Me._offValue = Me.Mask And Me.SizeMask
            End If

        End Set
    End Property

    ''' <summary>Gets or sets the bit pattern mask.</summary>
    ''' <value><c>Mask</c>is a Int64 bit pattern property</value>
    ''' <remarks>The mask is used to AND the bit value when determining if a value
    '''   is 'On' or 'Off'.</remarks>
    Public Property Mask() As Int64

    Private _offValue As Int64
    ''' <summary>Gets or sets the bit pattern that is interpreted as Off.</summary>
    ''' <value><c>OffValue</c>is a Int64 bit pattern property</value>
    ''' <remarks>A value is Off if it equals the OffValue when masked.</remarks>
    Public ReadOnly Property OffValue() As Int64
        Get
            Return Me._offValue
        End Get
    End Property

    Private _onValue As Int64
    ''' <summary>Gets or sets the bit pattern that is interpreted as On.</summary>
    ''' <value><c>OnValue</c>is a Int64 bit pattern property</value>
    ''' <remarks>A value is On if it equals the OnValue when masked.</remarks>
    Public ReadOnly Property OnValue() As Int64
        Get
            Return Me._onValue
        End Get
    End Property

    ''' <summary>Gets or sets the previous bit pattern value.</summary>
    ''' <value><c>PreviousValue</c>is a Int64 property</value>
    Public Property PreviousValue() As Int64
        Get
            Return Me.BitPatternElement.PreviousValue
        End Get
        Set(ByVal Value As Int64)
            Me.BitPatternElement.PreviousValue = Value
        End Set
    End Property

    Private _sizeMask As Int64
    ''' <summary>Gets or sets the size mask</summary>
    ''' <value><c>SizeMask</c> is a Boolean property.</value>
    ''' <remarks>To prevent negative values or shifting by more bits than the result can 
    '''   hold, Visual Basic, masks bit pattern values with a size mask  corresponding 
    '''   to the data type of pattern. The binary AND of these values is used 
    '''   for the shift amount. The size masks that visual basic uses are as follows:
    '''   <code>
    '''     Byte:     7F
    '''     Int16:    7FFF
    '''     Int32:  7FFF FFFF
    '''     Int64:     7FFF FFFF FFFF FFFF
    '''   </code>
    ''' </remarks>
    Public ReadOnly Property SizeMask() As Int64
        Get
            Return Me._sizeMask
        End Get
    End Property

    ''' <summary>Gets or sets the bit pattern value.</summary>
    ''' <value><c>Value</c>is a Int64 property</value>
    Public Property Value() As Int64
        Get
            Return Me.BitPatternElement.Value
        End Get
        Set(ByVal Value As Int64)
            Me.BitPatternElement.Value = Value
        End Set
    End Property

    ''' <summary>Gets toggled status of the bit pattern.</summary>
    ''' <value><c>WasToggled</c> is a Boolean property.</value>
    ''' <remarks>Is on if the previous bit pattern 
    '''   value was toggled with reference to the mask and the current 
    '''   bit pattern value.</remarks>
    Public ReadOnly Property WasToggled() As Boolean
        Get
            Return ((Me.BitPatternElement.Value Xor Me.BitPatternElement.PreviousValue) And Me.Mask) = Me.Mask
        End Get
    End Property

#End Region

#Region " PRIVATE  and  PROTECTED "

#End Region

#Region " ON EVENT HANDLERS "

    ''' <summary>Initializes the class elements.</summary>
    ''' <exception cref="isr.Core.Types.BaseException" guarantee="strong">
    '''   Failed initializing.</exception>
    ''' <remarks>Called from class constructors to initialize class element 
    '''   common to all constructors.</remarks>
    Private Sub onInstantiate(ByVal mask As Byte, ByVal PositiveLogic As Boolean)

        ' set the mask and logic
        Me._Mask = mask
        Me._sizeMask = &H7FFFFFFFFFFFFFFF
        Me.PositiveLogic = PositiveLogic

        onInstantiate()

    End Sub

    ''' <summary>Instantiates an Int32 bit pattern.</summary>
    ''' <param name="mask">A Int16 expression that specifies mask.</param>
    ''' <param name="PositiveLogic">A Boolean expression that specifies how
    '''   a bit pattern is interpreted as On or Off.  With positive logic, an on value
    '''   equals the bit pattern mask.</param>
    Private Sub onInstantiate(ByVal mask As Int16, ByVal PositiveLogic As Boolean)

        ' set the mask and logic
        Me._Mask = mask
        Me._sizeMask = &H7FFFFFFFFFFFFFFF
        Me.PositiveLogic = PositiveLogic

        onInstantiate()

    End Sub

    ''' <summary>Instantiates an Int32 bit pattern.</summary>
    ''' <param name="mask">A Int32 expression that specifies mask.</param>
    ''' <param name="PositiveLogic">A Boolean expression that specifies how
    '''   a bit pattern is interpreted as On or Off.  With positive logic, an on value
    '''   equals the bit pattern mask.</param>
    Private Sub onInstantiate(ByVal mask As Int32, ByVal PositiveLogic As Boolean)

        ' set the mask and logic
        Me._Mask = mask
        Me._sizeMask = &H7FFFFFFFFFFFFFFF
        Me.PositiveLogic = PositiveLogic

        onInstantiate()

    End Sub

    ''' <summary>Instantiates an Int32 bit pattern.</summary>
    ''' <param name="mask">A Int64 expression that specifies mask.</param>
    ''' <param name="PositiveLogic">A Boolean expression that specifies how
    '''   a bit pattern is interpreted as On or Off.  With positive logic, an on value
    '''   equals the bit pattern mask.</param>
    Private Sub onInstantiate(ByVal mask As Int64, ByVal PositiveLogic As Boolean)

        ' set the mask and logic
        Me._Mask = mask
        Me._sizeMask = &H7FFFFFFFFFFFFFFF
        Me.PositiveLogic = PositiveLogic

        onInstantiate()

    End Sub

    ''' <summary>Initializes the class elements.</summary>
    ''' <exception cref="isr.Core.Types.BaseException" guarantee="strong">
    '''   Failed initializing.</exception>
    ''' <remarks>Called from class constructors to initialize class element 
    '''   common to all constructors.</remarks>
    Private Sub onInstantiate()

        ' check if we have a negative mask
        If Me._Mask < 0 Then

            ' throw an exception
            Dim StatusMessage As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                        "{0} bit pattern mask must be non-negative", Me.GetType)
            Throw New isr.Core.Types.BaseException(StatusMessage)

        Else

            Try

                If Me._BitPatternElement Is Nothing Then
                    ' instantiate the internal bit pattern, which holds the bit pattern data
                    Me._BitPatternElement = New BitPatternElement

                    ' clear the value
                    Me._BitPatternElement.Value = Me._offValue
                End If

            Catch ex As System.Exception

                ' throw an exception
                Dim StatusMessage As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                            "{0} failed initializing", Me.GetType)
                Throw New isr.Core.Types.BaseException(StatusMessage, ex)

            End Try

        End If

    End Sub

#End Region

End Class

''' <summary>Handles a bit pattern data element.</summary>
''' <remarks>A reference instance is required for the bit pattern creating an object
'''   rather than a value type.  This way, a set of bit patterns can all share the same 
'''   main value by reference.</remarks>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="03/02/04" by="David" revision="1.0.1522.x">
''' Created
''' </history>
Public Class BitPatternElement

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    Public Sub New()

        ' instantiate the base class
        MyBase.New()

    End Sub

#End Region

#Region " PROPERTIES "

    Private _previousValue As Int64
    ''' <summary>Gets or sets the previous bit pattern value.</summary>
    ''' <value><c>Value</c>is a Int64 property</value>
    Public Property PreviousValue() As Int64
        Get
            Return Me._previousValue
        End Get
        Set(ByVal Value As Int64)
            If Value < 0 Then
                ' throw an exception
                Dim StatusMessage As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                            "{0} bit pattern value must be non-negative", Me.GetType)
                Throw New isr.Core.Types.BaseException(StatusMessage)
            Else
                Me._previousValue = Value
            End If
        End Set
    End Property

    Private _value As Int64
    ''' <summary>Gets or sets the bit pattern value.</summary>
    ''' <value><c>Value</c>is a Int64 bit pattern property</value>
    Public Property Value() As Int64
        Get
            Return Me._value
        End Get
        Set(ByVal Value As Int64)
            If Value < 0 Then
                ' throw an exception
                Dim StatusMessage As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                            "{0} bit pattern value must be non-negative", Me.GetType)
                Throw New isr.Core.Types.BaseException(StatusMessage)
            Else
                ' store the previous value
                Me._previousValue = Me._value
                ' update the new value
                Me._value = Value
            End If
        End Set
    End Property

#End Region

End Class

