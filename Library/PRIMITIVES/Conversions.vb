''' <summary>Provides type conversion methods.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/28/04" by="David" revision="1.0.1519.x">
''' Created
''' </history>
Public NotInheritable Class Conversions

    ''' <summary>Prevents instantiation of this type, which defines only static members.</summary>
    Private Sub New()
    End Sub

#Region " SHARED "

    ''' <summary>Code page to use for serializing</summary>
    Private Shared _encodingCodePage As System.Text.Encoding = System.Text.Encoding.UTF8

    ''' <summary>Returns a String value from the value object type.</summary>
    ''' <param name="dataValue">is an object expression that specifies the value which is to be 
    '''   converted</param>
    ''' <param name="dataFormat">A <see cref="System.String">String</see> expression that specifies the format of the converted value.</param>
    ''' <param name="dataType">A <see cref="System.String">String</see> expression that specifies the format of the converted value.</param>
    ''' <returns>The method returns a string value representing the data value to be 
    '''   converted.</returns>
    ''' <remarks>Use This method to get a string value from the input data.
    '''   <seealso cref="Serialize"/>is a preferred method.</remarks>
    Public Shared Function ConvertToString(ByVal dataValue As Object, ByVal dataFormat As String, ByVal dataType As Type) As String

        If dataType Is Nothing Then
            Return String.Empty
        End If

        If dataValue Is Nothing Then
            Return String.Empty
        End If

        Select Case Type.GetTypeCode(dataType)

            Case TypeCode.Empty, TypeCode.DBNull

                Return String.Empty

            Case TypeCode.DateTime

                Dim value As Date = CType(dataValue, System.DateTime)
                If dataFormat Is Nothing Then
                    Return value.ToString(Globalization.CultureInfo.CurrentCulture.NumberFormat)
                Else
                    If dataFormat.Length = 0 Then
                        Return value.ToString(Globalization.CultureInfo.CurrentCulture.DateTimeFormat)
                    Else
                        Return value.ToString(dataFormat, Globalization.CultureInfo.CurrentCulture.DateTimeFormat)
                    End If
                End If

            Case TypeCode.Decimal

                Dim value As Decimal = CType(dataValue, System.Decimal)
                If dataFormat Is Nothing Then
                    Return value.ToString(Globalization.CultureInfo.CurrentCulture.NumberFormat)
                Else
                    If dataFormat.Length = 0 Then
                        Return value.ToString(Globalization.CultureInfo.CurrentCulture.NumberFormat)
                    Else
                        Return value.ToString(dataFormat, Globalization.CultureInfo.CurrentCulture.NumberFormat)
                        '          Return Format(dataValue, dataFormat)
                    End If
                End If

            Case TypeCode.Double, TypeCode.Single

                Dim value As Double = CType(dataValue, System.Double)
                If dataFormat Is Nothing Then
                    Return value.ToString(Globalization.CultureInfo.CurrentCulture.NumberFormat)
                Else
                    If dataFormat.Length = 0 Then
                        Return value.ToString(Globalization.CultureInfo.CurrentCulture.NumberFormat)
                    Else
                        Return value.ToString(dataFormat, Globalization.CultureInfo.CurrentCulture.NumberFormat)
                        '          Return Format(dataValue, dataFormat)
                    End If
                End If

            Case Else

                ' use simple conversion otherwise
                Return dataValue.ToString()

        End Select

    End Function

    ''' <summary>Returns a value object from the object and object type.</summary>
    ''' <param name="value">is an Object expression that specifies the value which is 
    '''   to be converted</param>
    ''' <param name="dataType">A Type expression that specifies the system data type 
    '''   to create from the input string.</param>
    ''' <returns>Returns an Object value which value is defined by the 
    '''   string and its type is determined by the data type argument.</returns>
    ''' <remarks>Use This method to get a numeric value from a string expression.
    '''   This routine is not really an integral part of the Settings Class 
    '''   but rather necessary to allow us to show how different variable types 
    '''   can be used. <seealso cref="Deserialize"/>is a preferred method.</remarks>
    Public Shared Function ConvertToType(ByVal value As Object, ByVal dataType As Type) As Object

        ' Return a value per the given data type
        Select Case Type.GetTypeCode(dataType)
            Case TypeCode.Empty, TypeCode.DBNull
                Return CType(value, System.Object)
            Case TypeCode.Boolean
                Return CType(value, System.Boolean)
            Case TypeCode.Byte
                Return CType(value, System.Byte)
            Case TypeCode.Char
                Return CType(value, System.Char)
            Case TypeCode.DateTime
                Return CType(value, System.DateTime)
            Case TypeCode.Double
                Return CType(value, System.Double)
            Case TypeCode.Int16
                Return CType(value, System.Int16)
            Case TypeCode.Int32
                Return CType(value, System.Int32)
            Case TypeCode.Int64
                Return CType(value, System.Int64)
            Case TypeCode.Object
                Return CType(value, System.Object)
            Case TypeCode.Single
                Return CType(value, System.Single)
            Case TypeCode.String
                Return value
            Case Else
                Return CType(value, System.Object)
        End Select

    End Function

    ''' <summary>Returns a value object from the string and object type.</summary>
    ''' <param name="value">A <see cref="System.String">String</see> expression that specifies the value which is 
    '''   to be converted</param>
    ''' <param name="dataType">A Type expression that specifies the system data type 
    '''   to create from the input string.</param>
    ''' <returns>Returns an Object value which value is defined by the 
    '''   string and its type is determined by the data type argument.</returns>
    ''' <remarks>Use This method to get a numeric value from a string expression.
    '''   This routine is not really an integral part of the Settings Class 
    '''   but rather necessary to allow us to show how different variable types 
    '''   can be used. <seealso cref="Deserialize"/>is a preferred method.</remarks>
    Public Shared Function ConvertToType(ByVal value As String, ByVal dataType As Type) As Object

        ' Return a value per the given data type
        Select Case Type.GetTypeCode(dataType)
            Case TypeCode.Empty, TypeCode.DBNull
                Return CType(value, System.Object)
            Case TypeCode.Boolean
                Return CType(value, System.Boolean)
            Case TypeCode.Byte
                Return CType(value, System.Byte)
            Case TypeCode.Char
                Return CType(value, System.Char)
            Case TypeCode.DateTime
                Return CType(value, System.DateTime)
            Case TypeCode.Double
                Return CType(value, System.Double)
            Case TypeCode.Int16
                Return CType(value, System.Int16)
            Case TypeCode.Int32
                Return CType(value, System.Int32)
            Case TypeCode.Int64
                Return CType(value, System.Int64)
            Case TypeCode.Object
                Return CType(value, System.Object)
            Case TypeCode.Single
                Return CType(value, System.Single)
            Case TypeCode.String
                Return value
            Case Else
                Return CType(value, System.Object)
        End Select

    End Function

    ''' <summary>
    ''' Converts the string to an object
    ''' </summary>
    ''' <param name="value">A <see cref="System.String">String</see> serialized value</param>
    ''' <param name="defaultValue">Is an Object expression that specifies the default value</param>
    ''' <returns>Returns an Object value</returns>
    ''' <exception cref="System.Runtime.Serialization.SerializationException">Unable to deserialize value of specified object type.</exception>
    ''' <example>
    ''' This example shows how to handle data conversions to and from strings.
    '''   <code>
    ''' Sub Form_Click
    ''' Try
    ''' ' Convert a string to a given type
    ''' Dim defaultValue as Int32 = 0
    ''' Dim value As String = "10"
    ''' Dim outcome as Int32 = Conversions.Deserialize(value, defaultValue)
    ''' Dim outcome2 as Int32 = Conversions.Deserialize(value,System.Type.GetType("System.Int32"))
    ''' Catch e As System.Exception
    ''' ' respond to any file name errors.
    ''' system.Windows.Forms.MessageBox e.toString
    ''' End Try
    ''' End Sub  </code>
    ''' To run This example, paste the code fragment into a WinForm class.
    ''' Run the program by pressing F5, and then click on the form
    '''   </example>
    ''' <remarks>This method uses the type of the <paramref name="defaultValue">default value</paramref> to
    ''' set the returned value type</remarks>
    Public Shared Function Deserialize(ByVal value As String, ByVal defaultValue As Object) As Object

        If defaultValue Is Nothing Then

            Return value

        ElseIf TypeOf defaultValue Is DateTime Then

            Return deserializeDate(value)

        ElseIf TypeOf defaultValue Is IConvertible Then

            If defaultValue.GetType.IsEnum Then
                ' handle enumeration separately
                Return System.Enum.Parse(defaultValue.GetType, value)
            Else
                ' all other convertible (primitive) types
                Return System.Convert.ChangeType(value, defaultValue.GetType, Globalization.CultureInfo.CurrentCulture)
            End If

        ElseIf TypeOf defaultValue Is ISerializable OrElse
               TypeOf defaultValue Is System.ValueType OrElse
               TypeOf defaultValue Is System.Array OrElse
                      defaultValue Is Nothing Then

            ' Serializable object, structures, and arrays handled here.
            ' Also assume Nothing resolves to a serialized "thing".
            Return deserializeOther(value, defaultValue)

        Else
            ' Here the developer is requesting data that CAN'T
            ' be serialized or deserialized.  That's a coding problem!
            Throw New System.Runtime.Serialization.SerializationException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                                        "Unable to deserialize value of type '{0}'.",
                                                                                        defaultValue.GetType.ToString))
        End If

    End Function

    ''' <summary>
    ''' Converts the string to an object.
    ''' </summary>
    ''' <param name="value">A <see cref="System.String">String</see> serialized value</param>
    ''' <param name="type">Specifies a type for the value.</param>
    ''' <returns>Returns an Object value</returns>
    ''' <exception cref="System.Runtime.Serialization.SerializationException">Unhanded type.</exception>
    ''' <remarks>This method uses the type of the <paramref name="defaultValue">default value</paramref> to
    ''' set the returned value type</remarks>
    Public Shared Function Deserialize(ByVal value As String, ByVal type As System.Type) As Object

        If type Is Nothing Then

            Return Nothing

        ElseIf type Is GetType(System.DateTime) Then

            Return deserializeDate(value)

        ElseIf type.IsEnum Then '  GetType(IConvertible) Then

            ' handle enumeration separately
            Return System.Enum.Parse(type, value)

        ElseIf type.IsPrimitive OrElse type.Equals(GetType(System.Decimal)) Then

            ' all other convertible (primitive) types
            Return System.Convert.ChangeType(value, type, Globalization.CultureInfo.CurrentCulture)

        ElseIf type.Equals(GetType(System.String)) Then

            Return value

        ElseIf type.IsSerializable OrElse
               type.IsValueType OrElse type.IsArray Then

            ' Serializable object, structures, and arrays handled here.
            ' Also assume Nothing resolves to a serialized "thing".
            Return deserializeOther(value, Nothing)

        Else
            ' Here the developer is requesting data that CAN'T
            ' be serialized or deserialized.  That's a coding problem!
            Throw New Runtime.Serialization.SerializationException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                                 "Unhanded value of type '{0}'.", type.Name))
        End If

    End Function

    ''' <summary>Deserialize structures and arrays handled here.</summary>
    ''' <param name="value">A <see cref="System.String">String</see> serialized value</param>
    ''' <param name="defaultValue">Is an Object expression that specifies the default value</param>
    Private Shared Function deserializeOther(ByVal value As String, ByVal defaultValue As Object) As Object

        If value Is Nothing Then
            Return defaultValue
        End If

        ' Serializable object, structures, and arrays handled here.
        ' Also assume Nothing resolves to a serialized "thing".
        ' Restore value from stream:
        Dim formatter As New System.Runtime.Serialization.Formatters.Soap.SoapFormatter
        'Dim formatter As New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
        Try
            Using stream As New System.IO.MemoryStream(Conversions._encodingCodePage.GetBytes(value))
                Return formatter.Deserialize(stream)
            End Using
        Catch ex As System.Runtime.Serialization.SerializationException
            ' Can't deserialize to target type.
            ' Probably because typedef has been changed.
            ' I could throw an error here, but I'd rather just
            ' ignore the saved data and continue.
            Return defaultValue
        End Try

    End Function

    ''' <summary>Deserializes a date value.</summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Shared Function deserializeDate(ByVal value As String) As Date

        Try

            ' use this to convert from tick count.
            Dim a As Double
            If System.Double.TryParse(value, Globalization.NumberStyles.Integer, Globalization.CultureInfo.CurrentCulture, a) Then
                Return New DateTime(System.Convert.ToInt64(value, Globalization.CultureInfo.CurrentCulture))
            Else
                Try
                    ' With This we can convert normal dates
                    Return CType(value, System.DateTime)
                Catch
                    ' With This we can convert normal dates
                    Return DateTime.Parse(value, Globalization.CultureInfo.CurrentCulture)
                End Try
            End If

        Catch ex As System.FormatException

            Try

                ' With this we can convert normal dates
                Return CType(value, System.DateTime)

            Catch

                ' With this we can convert normal dates
                Return DateTime.Parse(value, Globalization.CultureInfo.CurrentCulture)

            End Try

        Catch ex As System.InvalidCastException

            Try

                ' With this we can convert normal dates
                Return CType(value, System.DateTime)

            Catch

                ' With this we can convert normal dates
                Return DateTime.Parse(value, Globalization.CultureInfo.CurrentCulture)

            End Try

        End Try

    End Function

    ''' <summary>Checks if an object is Value Type object.</summary>
    ''' <param name="ThisItem">
    '''   specifies an instance of an object</param>
    ''' <returns>A Boolean data type</returns>
    ''' <remarks>Use This method to check if an object is Value type.</remarks>
    Public Shared Function IsValueType(ByVal thisItem As Object) As Boolean
        If TypeOf (thisItem) Is System.ValueType Then
            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary>Converts an object value to string.</summary>
    ''' <param name="value">Is an Object expression that specifies the value to convert</param>
    ''' <remarks>Use This method to convert the object value to string for storing as
    '''   an application settings.  Note that date time types are saved as ticks to
    '''   preserve partial second accuracy.  Objects that are not IConvertible are
    '''   serialized.</remarks>
    Public Shared Function Serialize(ByVal value As Object) As String

        If value Is Nothing Then

            Return String.Empty

        ElseIf TypeOf value Is DateTime Then

            Try

                ' use ticks; convert only returns seconds.
                Return CType(value, DateTime).Ticks.ToString(Globalization.CultureInfo.CurrentCulture)

            Catch ex As System.InvalidCastException

                ' convert primitive to string representation.
                Return CType(System.Convert.ChangeType(value, TypeCode.String, Globalization.CultureInfo.CurrentCulture), String)

            End Try

        ElseIf TypeOf value Is IConvertible Then

            ' convert primitive or enumeration to string representation.
            Return CType(System.Convert.ChangeType(value, TypeCode.String, Globalization.CultureInfo.CurrentCulture), String)

        ElseIf TypeOf value Is ISerializable OrElse
               TypeOf value Is System.ValueType OrElse
               TypeOf value Is System.Array Then

            ' Serializable object, structure, or array serialize object's data into string
            Dim formatter As New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter

            Try
                Using stream As System.IO.MemoryStream = New System.IO.MemoryStream
                    formatter.Serialize(stream, value)
                    Return Conversions._encodingCodePage.GetString(stream.GetBuffer())
                End Using
            Catch ex As System.Runtime.Serialization.SerializationException

                ' probably an array of unsupported (not serializable) objects.
                Throw New System.Runtime.Serialization.SerializationException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                                            "Unable to serialize value of type '{0}'.",
                                                                                            value.GetType.ToString))

            End Try

        Else

            ' Can't serialize: programmer error!
            Throw New System.Runtime.Serialization.SerializationException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                                        "Unhandled value of type '{0}'.",
                                                                                        value.GetType.ToString))

        End If

    End Function

#End Region

End Class

