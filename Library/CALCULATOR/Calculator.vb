Namespace Calculable

    ''' <summary>
    ''' Defines the interface for the generic calculator class.
    ''' </summary>
    ''' <typeparam name="T">Specifies the type parameter on a generic class</typeparam>
    Public Interface ICalculator(Of T)

        Function Add(ByVal left As T, ByVal right As T) As T
        Function Subtract(ByVal left As T, ByVal right As T) As T
        Function Multiply(ByVal left As T, ByVal right As T) As T
        Function Multiply(ByVal left As Object, ByVal right As T) As T
        Function Divide(ByVal left As T, ByVal right As T) As T
        Function Hypotenuse(ByVal left As T, ByVal right As T) As T
        Function MaximumNorm(ByVal left As T, ByVal right As T) As T
        Function ToDouble(ByVal value As T) As Double
        Function FromDouble(ByVal value As Double) As T

    End Interface

    ''' <summary>
    ''' Implements a generic calculator for <see cref="System.Int32">Int32</see> types.
    ''' </summary>
    Public Structure CalculatorI
        Implements ICalculator(Of Int32)

        ''' <summary>
        ''' A dummy element required for the integrity of the structure.
        ''' </summary>
        Private _dummy As Int32

        ''' <summary>
        ''' Returns a hash code for this instance.
        ''' </summary><returns>
        ''' A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        ''' </returns>
        Public Overloads Overrides Function GetHashCode() As Int32
            Return Me._dummy.GetHashCode
        End Function

        ''' <summary>
        ''' Defines equals for <see cref="CalculatorI">CalculatorI</see>.
        ''' </summary>
        Public Overrides Function Equals(ByVal obj As Object) As Boolean
            Return Object.ReferenceEquals(Me, obj) OrElse CalculatorI.Equals(Me, CType(obj, CalculatorI))
        End Function

        ''' <summary>
        ''' Defines equals for <see cref="CalculatorI">CalculatorI</see>.
        ''' </summary>
        Public Overloads Function Equals(ByVal value As CalculatorI) As Boolean
            Return CalculatorI.Equals(Me, value)
        End Function

        ''' <summary>
        ''' Defines addition operation for <see cref="System.Int32">Int32</see>.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the left hand side argument of the binary operation.</param>
        ''' <returns>The sum of the <paramref name="left">first</paramref> and 
        ''' <paramref name="right">second</paramref> arguments.</returns>
        Public Function Add(ByVal left As Int32, ByVal right As Int32) As Int32 Implements ICalculator(Of Int32).Add
            Return left + right
        End Function

        ''' <summary>
        ''' Defines equals for <see cref="System.Int32">Int32</see>.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        ''' <returns>True if the <paramref name="left">first</paramref> and 
        ''' <paramref name="right">second</paramref> arguments are equals; false otherwise.</returns>
        Public Overloads Shared Function Equals(ByVal left As Int32, ByVal right As Int32) As Boolean
            Return left.Equals(right)
        End Function

        ''' <summary>
        ''' Defines equals for <see cref="System.Int32">Int32</see>.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        ''' <returns>True if the <paramref name="left">first</paramref> and 
        ''' <paramref name="right">second</paramref> arguments are equals; false otherwise.</returns>
        Public Overloads Shared Function Equals(ByVal left As CalculatorI, ByVal right As CalculatorI) As Boolean
            Return left.Equals(right)
        End Function

        ''' <summary>
        ''' Defines '==' operator.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        Public Shared Operator =(ByVal left As CalculatorI, ByVal right As CalculatorI) As Boolean
            Return left.Equals(right)
        End Operator

        ''' <summary>
        ''' Defines  '!=' operator.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        Public Shared Operator <>(ByVal left As CalculatorI, ByVal right As CalculatorI) As Boolean
            Return Not left.Equals(right)
        End Operator

        ''' <summary>
        ''' Defines subtraction operation for <see cref="System.Int32">Int32</see>.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        ''' <returns>The sum of the <paramref name="left">first</paramref> and 
        ''' <paramref name="right">second</paramref> arguments.</returns>
        Public Function Subtract(ByVal left As Int32, ByVal right As Int32) As Int32 Implements ICalculator(Of Int32).Subtract
            Return left - right
        End Function

        ''' <summary>
        ''' Defines multiplication operation for <see cref="System.Int32">Int32</see>.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        ''' <returns>The sum of the <paramref name="left">first</paramref> and 
        ''' <paramref name="right">second</paramref> arguments.</returns>
        Public Function Multiply(ByVal left As Int32, ByVal right As Int32) As Int32 Implements ICalculator(Of Int32).Multiply
            Return left * right
        End Function

        ''' <summary>
        ''' Defines multiplication operation for <see cref="System.Int32">Int32</see>.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        ''' <returns>The sum of the <paramref name="left">first</paramref> and 
        ''' <paramref name="right">second</paramref> arguments.</returns>
        Public Function Multiply(ByVal left As Object, ByVal right As Int32) As Int32 Implements ICalculator(Of Int32).Multiply
            Return CInt(CDbl(left) * right)
        End Function

        ''' <summary>
        ''' Defines division operation for <see cref="System.Int32">Int32</see>.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        ''' <returns>The sum of the <paramref name="left">first</paramref> and 
        ''' <paramref name="right">second</paramref> arguments.</returns>
        Public Function Divide(ByVal left As Int32, ByVal right As Int32) As Int32 Implements ICalculator(Of Int32).Divide
            Return left \ right
        End Function

        ''' <summary>
        ''' Calculates the square root of (a^2 + b^2) without under/overflow.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        ''' <returns>Int32.</returns>
        Public Function Hypotenuse(ByVal left As Int32, ByVal right As Int32) As Int32 Implements ICalculator(Of Int32).Hypotenuse
            Dim r As Double
            If Math.Abs(left) > Math.Abs(right) Then
                r = right / left
                r = Math.Abs(left) * Math.Sqrt((1 + r * r))
            ElseIf right <> 0 Then
                r = left / right
                r = Math.Abs(right) * Math.Sqrt((1 + r * r))
            Else
                r = 0.0
            End If
            Return CInt(r)
        End Function

        ''' <summary>
        ''' Returns the maximum norm of the range, i.e., the maximum absolute value of the range.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        ''' <returns>Int32.</returns>
        Public Function MaximumNorm(ByVal left As Int32, ByVal right As Int32) As Int32 Implements ICalculator(Of Integer).MaximumNorm
            Return Math.Max(Math.Abs(left), Math.Abs(right))
        End Function

        ''' <summary>
        ''' Returns the Double representation of the specified value.
        ''' </summary>
        ''' <param name="value">The value.</param>
        ''' <returns>System.Double.</returns>
        Public Function ToDouble(ByVal value As Int32) As Double Implements ICalculator(Of Integer).ToDouble
            Return CDbl(value)
        End Function

        ''' <summary>
        ''' Returns the Integer value representation of the double value.
        ''' </summary>
        ''' <param name="value">The value.</param>
        ''' <returns>Int32.</returns>
        Public Function FromDouble(ByVal value As Double) As Int32 Implements ICalculator(Of Integer).FromDouble
            Return CInt(value)
        End Function

    End Structure

    ''' <summary>
    ''' Implements a generic calculator for <see cref="System.Double">Double</see> types.
    ''' </summary>
    Public Structure CalculatorR
        Implements ICalculator(Of Double)

        ''' <summary>
        ''' A dummy element required for the integrity of the structure.
        ''' </summary>
        Private _dummy As Double

        ''' <summary>
        ''' Returns a hash code for this instance.
        ''' </summary><returns>
        ''' A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        ''' </returns>
        Public Overloads Overrides Function GetHashCode() As Int32
            Return Me._dummy.GetHashCode
        End Function

        ''' <summary>
        ''' Defines the addition operation for <see cref="System.Double">Doubles</see>.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        ''' <returns>The sum of the <paramref name="left">first</paramref> and 
        ''' <paramref name="right">second</paramref> arguments.</returns>
        Public Function Add(ByVal left As Double, ByVal right As Double) As Double Implements ICalculator(Of Double).Add
            Return left + right
        End Function

        ''' <summary>
        ''' Defines equals for <see cref="System.Double">Double</see>.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        ''' <returns>True if the <paramref name="left">first</paramref> and 
        ''' <paramref name="right">second</paramref> arguments are equals; false otherwise.</returns>
        Public Overloads Shared Function Equals(ByVal left As Double, ByVal right As Double) As Boolean
            Return left.Equals(right)
        End Function

        ''' <summary>
        ''' Defines equals for <see cref="CalculatorR">CalculatorR</see>.
        ''' </summary>
        Public Overrides Function Equals(ByVal obj As Object) As Boolean
            Return Object.ReferenceEquals(Me, obj) OrElse CalculatorR.Equals(Me, CType(obj, CalculatorR))
        End Function

        ''' <summary>
        ''' Defines equals for <see cref="CalculatorR">CalculatorR</see>.
        ''' </summary>
        Public Overloads Function Equals(ByVal value As CalculatorR) As Boolean
            Return CalculatorI.Equals(Me, value)
        End Function

        ''' <summary>
        ''' Defines equals for <see cref="CalculatorR">CalculatorR</see>.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        ''' <returns>True if the <paramref name="left">first</paramref> and 
        ''' <paramref name="right">second</paramref> arguments are equals; false otherwise.</returns>
        Public Overloads Shared Function Equals(ByVal left As CalculatorR, ByVal right As CalculatorR) As Boolean
            Return left.Equals(right)
        End Function

        ''' <summary>
        ''' Defines '==' operator.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        Public Shared Operator =(ByVal left As CalculatorR, ByVal right As CalculatorR) As Boolean
            Return left.Equals(right)
        End Operator

        ''' <summary>
        ''' Defines  '!=' operator.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        Public Shared Operator <>(ByVal left As CalculatorR, ByVal right As CalculatorR) As Boolean
            Return Not left.Equals(right)
        End Operator

        ''' <summary>
        ''' Defines subtraction operation for <see cref="System.Double">Doubles</see>.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        ''' <returns>The sum of the <paramref name="left">first</paramref> and 
        ''' <paramref name="right">second</paramref> arguments.</returns>
        Public Function Subtract(ByVal left As Double, ByVal right As Double) As Double Implements ICalculator(Of Double).Subtract
            Return left - right
        End Function

        ''' <summary>
        ''' Defines multiplication operation for <see cref="System.Double">Double</see>.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        ''' <returns>The sum of the <paramref name="left">first</paramref> and 
        ''' <paramref name="right">second</paramref> arguments.</returns>
        Public Function Multiply(ByVal left As Double, ByVal right As Double) As Double Implements ICalculator(Of Double).Multiply
            Return left * right
        End Function

        ''' <summary>
        ''' Defines multiplication operation for <see cref="System.Double">Double</see>.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        ''' <returns>The sum of the <paramref name="left">first</paramref> and 
        ''' <paramref name="right">second</paramref> arguments.</returns>
        Public Function Multiply(ByVal left As Object, ByVal right As Double) As Double Implements ICalculator(Of Double).Multiply
            Return CDbl(left) * right
        End Function

        ''' <summary>
        ''' Defines division operation for <see cref="System.Double">Double</see>.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        ''' <returns>The sum of the <paramref name="left">first</paramref> and 
        ''' <paramref name="right">second</paramref> arguments.</returns>
        Public Function Divide(ByVal left As Double, ByVal right As Double) As Double Implements ICalculator(Of Double).Divide
            Return left / right
        End Function

        ''' <summary>
        ''' Calculates square root of (a^2 + b^2) without under/overflow.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        ''' <returns>System.Double.</returns>
        Public Function Hypotenuse(ByVal left As Double, ByVal right As Double) As Double Implements ICalculator(Of Double).Hypotenuse
            Dim r As Double
            If Math.Abs(left) > Math.Abs(right) Then
                r = right / left
                r = Math.Abs(left) * Math.Sqrt((1 + r * r))
            ElseIf right <> 0 Then
                r = left / right
                r = Math.Abs(right) * Math.Sqrt((1 + r * r))
            Else
                r = 0.0
            End If
            Return r
        End Function

        ''' <summary>
        ''' Returns the maximum norm of the range, i.e., the maximum absolute value of the range.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        ''' <returns>System.Double.</returns>
        Public Function MaximumNorm(ByVal left As Double, ByVal right As Double) As Double Implements ICalculator(Of Double).MaximumNorm
            Return Math.Max(Math.Abs(left), Math.Abs(right))
        End Function

        ''' <summary>
        ''' Returns the Double representation of the specified value.
        ''' </summary>
        ''' <param name="value">The value.</param>
        ''' <returns>System.Double.</returns>
        Public Function ToDouble(ByVal value As Double) As Double Implements ICalculator(Of Double).ToDouble
            Return value
        End Function

        ''' <summary>
        ''' Returns the Integer value representation of the double value.
        ''' </summary>
        ''' <param name="value">The value.</param>
        ''' <returns>System.Double.</returns>
        Public Function FromDouble(ByVal value As Double) As Double Implements ICalculator(Of Double).FromDouble
            Return value
        End Function

    End Structure

    ''' <summary> Defines a generic number that is amenable to the generic Calculator algebra. </summary>
    ''' <license> (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    ''' SOFTWARE.</para> </license>
    ''' <typeparam name="TCalculator"> The type of the T calculator. </typeparam>
    Public Structure Number(Of T, TCalculator As {ICalculator(Of T), New})

        ''' <summary>
        ''' Holds the internal value of the number.
        ''' </summary>
        Private _value As T

        ''' <summary>
        ''' Holds the calculator applied on the number.
        ''' </summary>
        Private Shared _calculator As TCalculator

        ''' <summary>
        ''' Constructor using a value if the generic type.
        ''' </summary>
        ''' <param name="value">Specifies the initial value.</param>
        Public Sub New(ByVal value As T)
            Me._value = value
        End Sub

        ''' <summary>
        ''' Constructor using a <see cref="System.Double">Value</see>.
        ''' </summary>
        ''' <param name="value">Specifies the initial value.</param>
        Public Sub New(ByVal value As Double)
            Me._value = CType(value, Number(Of T, TCalculator))
        End Sub

        ''' <summary> Returns the value. </summary>
        ''' <value> The value. </value>
        Public ReadOnly Property Value() As T
            Get
                Return Me._value
            End Get
        End Property

        ''' <summary>
        ''' Returns a hash code for this instance.
        ''' </summary><returns>
        ''' A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        ''' </returns>
        Public Overloads Overrides Function GetHashCode() As Int32
            Return Me.Value.GetHashCode
        End Function

        ''' <summary>
        ''' Defines equals for <see cref="CalculatorI">CalculatorI</see>.
        ''' </summary>
        Public Overrides Function Equals(ByVal obj As Object) As Boolean
            Return Object.ReferenceEquals(Me, obj) OrElse CalculatorI.Equals(Me, CType(obj, CalculatorI))
        End Function

        ''' <summary>
        ''' Defines equals for <see cref="CalculatorI">CalculatorI</see>.
        ''' </summary>
        Public Overloads Function Equals(ByVal value As CalculatorI) As Boolean
            Return CalculatorI.Equals(Me, value)
        End Function

        ''' <summary>
        ''' Casts from the generic type into the <see cref="Number(Of T, TC)">Number</see> type.
        ''' </summary>
        ''' <param name="value">The value.</param>
        ''' <returns>The result of the conversion.</returns>
        Public Shared Widening Operator CType(ByVal value As T) As Number(Of T, TCalculator)
            Return Number(Of T, TCalculator).FromType(value)
        End Operator

        ''' <summary>
        ''' Casts from the generic type into the <see cref="Number(Of T, TC)">Number</see> type.
        ''' </summary>
        ''' <param name="value">The value.</param>
        ''' <returns>Number{`0`1}.</returns>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Shared Function FromType(ByVal value As T) As Number(Of T, TCalculator)
            Number(Of T, TCalculator)._calculator = New TCalculator()
            Return New Number(Of T, TCalculator)(value)
        End Function

        ''' <summary>
        ''' Casts the <see cref="Number(Of T, TC)">Number</see> type into the generic type.
        ''' </summary>
        ''' <param name="value">The value.</param>
        ''' <returns>The result of the conversion.</returns>
        Public Shared Widening Operator CType(ByVal value As Number(Of T, TCalculator)) As T
            Return Number(Of T, TCalculator).ToType(value)
        End Operator

        ''' <summary>
        ''' Casts the <see cref="Number(Of T, TC)">Number</see> type into the generic type.
        ''' </summary>
        ''' <param name="value">The value.</param>
        ''' <returns>`0.</returns>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Shared Function ToType(ByVal value As Number(Of T, TCalculator)) As T
            Return value._value
        End Function

        ''' <summary>
        ''' Casts the <see cref="Number(Of T, TC)">Number</see> type to <see cref="System.Double">Double</see> type.
        ''' </summary>
        ''' <param name="value">The value.</param>
        ''' <returns>The result of the conversion.</returns>
        Public Shared Widening Operator CType(ByVal value As Number(Of T, TCalculator)) As Double
            Return Number(Of T, TCalculator).ToDouble(value)
        End Operator

        ''' <summary>
        ''' Casts the <see cref="Number(Of T, TC)">Number</see> type to <see cref="System.Double">Double</see> type.
        ''' </summary>
        ''' <param name="value">The value.</param>
        ''' <returns>System.Double.</returns>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Shared Function ToDouble(ByVal value As Number(Of T, TCalculator)) As Double
            Return Number(Of T, TCalculator)._calculator.ToDouble(value.Value)
        End Function

        ''' <summary>
        ''' Casts from the <see cref="System.Double">Double</see> type to <see cref="Number(Of T, TC)">Number</see> type.
        ''' </summary>
        ''' <param name="value">The value.</param>
        ''' <returns>The result of the conversion.</returns>
        Public Shared Widening Operator CType(ByVal value As Double) As Number(Of T, TCalculator)
            Return Number(Of T, TCalculator).FromDouble(value)
        End Operator

        ''' <summary>
        ''' Casts from the <see cref="System.Double">Double</see> type to <see cref="Number(Of T, TC)">Number</see> type.
        ''' </summary>
        ''' <param name="value">The value.</param>
        ''' <returns>Number{`0`1}.</returns>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Shared Function FromDouble(ByVal value As Double) As Number(Of T, TCalculator)
            Number(Of T, TCalculator)._calculator = New TCalculator()
            Return New Number(Of T, TCalculator)(Number(Of T, TCalculator)._calculator.FromDouble(value))
        End Function

        ''' <summary>
        ''' Defines addition.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Shared Function Add(ByVal left As Number(Of T, TCalculator), ByVal right As Number(Of T, TCalculator)) As Number(Of T, TCalculator)
            Return Number(Of T, TCalculator)._calculator.Add(left.Value, right.Value)
        End Function

        ''' <summary>
        ''' Defines left '+' operator on the number.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        Public Shared Operator +(ByVal left As Number(Of T, TCalculator), ByVal right As Number(Of T, TCalculator)) As Number(Of T, TCalculator)
            Return Number(Of T, TCalculator).Add(left, right)
        End Operator

        ''' <summary>
        ''' Defines subtraction.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Shared Function Subtract(ByVal left As Number(Of T, TCalculator), ByVal right As Number(Of T, TCalculator)) As Number(Of T, TCalculator)
            Return Number(Of T, TCalculator)._calculator.Subtract(left.Value, right.Value)
        End Function

        ''' <summary>
        ''' Defines equals on the number.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Overloads Shared Function Equals(ByVal left As Number(Of T, TCalculator), ByVal right As Number(Of T, TCalculator)) As Boolean
            Return Number(Of T, TCalculator).Equals(left, right)
        End Function

        ''' <summary>
        ''' Defines '=' operator on the number.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        Public Shared Operator =(ByVal left As Number(Of T, TCalculator), ByVal right As Number(Of T, TCalculator)) As Boolean
            Return Number(Of T, TCalculator).Equals(left, right)
        End Operator

        ''' <summary>
        ''' Defines 'NE' operator on the number.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        Public Shared Operator <>(ByVal left As Number(Of T, TCalculator), ByVal right As Number(Of T, TCalculator)) As Boolean
            Return Not Number(Of T, TCalculator).Equals(left, right)
        End Operator

        ''' <summary>
        ''' Defines left '-' operator on the number.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        Public Shared Operator -(ByVal left As Number(Of T, TCalculator), ByVal right As Number(Of T, TCalculator)) As Number(Of T, TCalculator)
            Return Number(Of T, TCalculator).Subtract(left, right)
        End Operator

        ''' <summary>
        ''' Defines multiplication.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Shared Function Multiply(ByVal left As Number(Of T, TCalculator), ByVal right As Number(Of T, TCalculator)) As Number(Of T, TCalculator)
            Return Number(Of T, TCalculator)._calculator.Multiply(left.Value, right.Value)
        End Function

        ''' <summary>
        ''' Defines a '*' operator on the number.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        Public Shared Operator *(ByVal left As Number(Of T, TCalculator), ByVal right As Number(Of T, TCalculator)) As Number(Of T, TCalculator)
            Return Number(Of T, TCalculator).Multiply(left, right)
        End Operator

        ''' <summary>
        ''' Defines double multiplication.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Shared Function Multiply(ByVal left As Object, ByVal right As Number(Of T, TCalculator)) As Number(Of T, TCalculator)
            Return Number(Of T, TCalculator)._calculator.Multiply(left, right.Value)
        End Function

        ''' <summary>
        ''' Defines a '*' operator on the number.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        Public Shared Operator *(ByVal left As Object, ByVal right As Number(Of T, TCalculator)) As Number(Of T, TCalculator)
            Return Number(Of T, TCalculator).Multiply(left, right)
        End Operator

        ''' <summary>
        ''' Defines division.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Shared Function Divide(ByVal left As Number(Of T, TCalculator), ByVal right As Number(Of T, TCalculator)) As Number(Of T, TCalculator)
            Return Number(Of T, TCalculator)._calculator.Divide(left.Value, right.Value)
        End Function

        ''' <summary>
        ''' Defines a '/' operator on the number.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        Public Shared Operator /(ByVal left As Number(Of T, TCalculator), ByVal right As Number(Of T, TCalculator)) As Number(Of T, TCalculator)
            Return Number(Of T, TCalculator).Divide(left, right)
        End Operator

        ''' <summary>
        ''' Defines a Hypotenuse operator on the number.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        ''' <returns>Number{`0`1}.</returns>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Shared Function Hypotenuse(ByVal left As Number(Of T, TCalculator), ByVal right As Number(Of T, TCalculator)) As Number(Of T, TCalculator)
            Return Number(Of T, TCalculator)._calculator.Hypotenuse(left.Value, right.Value)
        End Function

        ''' <summary>
        ''' Defines a Maximum Norm operator on the number.
        ''' </summary>
        ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
        ''' <param name="right">Specifies the right hand side argument of the binary operation.</param>
        ''' <returns>Number{`0`1}.</returns>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Shared Function MaximumNorm(ByVal left As Number(Of T, TCalculator), ByVal right As Number(Of T, TCalculator)) As Number(Of T, TCalculator)
            Return Number(Of T, TCalculator)._calculator.MaximumNorm(left.Value, right.Value)
        End Function

    End Structure

End Namespace
