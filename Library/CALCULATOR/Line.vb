﻿Namespace Calculable

    ''' <summary>
    ''' Implements a generic line class capable of computing slope and offset or points on the line.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <typeparam name="TCalculator">The type of the T calculator.</typeparam>
    Public Class Line(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable}, TCalculator As {ICalculator(Of T), New})

        Inherits isr.Core.Types.Line(Of T)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs a unity <see cref="Line(of t, TC)"/> instance.</summary>
        Public Sub New()

            Me.new(Line(Of T, TCalculator).Unity)

        End Sub

        ''' <summary>The Copy Constructor</summary>
        ''' <param name="model">The <see cref="Line">Line</see> object from which to Copy</param>
        Public Sub New(ByVal model As Line(Of T, TCalculator))

            MyBase.New()
            If model IsNot Nothing Then
                MyBase.SetLine(model.X1, model.Y1, model.X2, model.Y2)
                Me.UpdateSlopeOffset()
            End If

        End Sub

        ''' <summary>
        ''' Constructs this class.
        ''' </summary>
        ''' <param name="x1">Specifies the X1 coordinate of the line.</param>
        ''' <param name="y1">Specifies the Y1 coordinate of the line.</param>
        ''' <param name="x2">Specifies the X2 coordinate of the line.</param>
        ''' <param name="y2">Specifies the Y2 coordinate of the line.</param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
        Public Sub New(ByVal x1 As T, ByVal y1 As T, ByVal x2 As T, ByVal y2 As T)

            MyBase.new(x1, y1, x2, y2)

        End Sub

        ''' <summary>Constructs a <see cref="LineI"/> instance by its slope and offset.</summary>
        ''' <param name="slope">A <see cref="System.Double">Double</see> expression that specifies the slope
        '''   of the line.</param>
        ''' <param name="offset">A <see cref="System.Double">Double</see> expression that specifies the offset
        '''   of the line.</param>
        Public Sub New(ByVal slope As Double, ByVal offset As Double)
            Me.new()
            Me.SetLine(slope, offset)
        End Sub

#End Region

#Region " SHARED "

        ''' <summary>Returns the calculates offset, i.e., the pressure at zero volts.</summary>
        ''' <param name="x1">The X1 coordinate of the line.</param>
        ''' <param name="slope">The slope of the line.</param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Shared Function ComputeOffset(ByVal x1 As T, ByVal y1 As T, ByVal slope As Double) As Double
            Return y1 - slope * New Number(Of T, TCalculator)(x1)
        End Function

        ''' <summary>
        ''' Returns the offset calculated for the specified line.  The slope must
        ''' be calculated first.
        ''' </summary>
        ''' <param name="line">Specifies the <see cref="Line(Of T, TC)">line</see>.</param>
        Private Shared Function ComputeOffset(ByVal line As Line(Of T, TCalculator)) As Double
            Return isr.Core.Types.Calculable.Line(Of T, TCalculator).ComputeOffset(line.X1, line.Y1, line.Slope)
        End Function

        ''' <summary>
        ''' Returns the slope calculated for the specified line.
        ''' </summary>
        ''' <param name="line">Specifies the <see cref="Line(Of T, TC)">line</see>.</param>
        Private Shared Function ComputeSlope(ByVal line As Line(Of T, TCalculator)) As Double
            Return isr.Core.Types.Calculable.Line(Of T, TCalculator).ComputeSlope(line.X1, line.Y1, line.X2, line.Y2)
        End Function

        ''' <summary>
        ''' Returns the calculated slope as the ratio of Y over the X ranges.</summary>
        ''' <param name="x1">Specifies the X1 coordinate of the line.</param>
        ''' <param name="y1">Specifies the Y1 coordinate of the line.</param>
        ''' <param name="x2">Specifies the X2 coordinate of the line.</param>
        ''' <param name="y2">Specifies the Y2 coordinate of the line.</param>
        Private Shared Function ComputeSlope(ByVal x1 As T, ByVal y1 As T, ByVal x2 As T, ByVal y2 As T) As Double

            Return (New Number(Of T, TCalculator)(y2) - New Number(Of T, TCalculator)(y1)) /
                   (New Number(Of T, TCalculator)(x2) - New Number(Of T, TCalculator)(x1))

        End Function

        ''' <summary>
        ''' Compares two lines. The lines are compared using their slopes and offsets.</summary>
        ''' <param name="left">Specifies the <see cref="Line(of T, TC)">Line</see>
        '''   to compare with.</param>
        ''' <param name="right">Specifies the <see cref="Line(of T, TC)">Line</see>
        '''   to compare to.</param>
        ''' <param name="tolerance">Specifies the relative tolerance for comparing the two
        '''   lines.  The lines are compared based on their slope and offset.  The offset
        '''   tolerance is based on it relative change from the Y range.  The tolerance if based on
        '''   the reference line.</param>
        ''' <returns>A Boolean data type</returns>
        ''' <remarks>The two lines are the same if the have the same minimum and maximum
        '''   values.</remarks>
        Private Overloads Shared Function Equals(ByVal left As Line(Of T, TCalculator), ByVal right As Line(Of T, TCalculator), ByVal tolerance As Double) As Boolean
            If left Is Nothing Then
                Return right Is Nothing
            ElseIf right Is Nothing Then
                Return False
            Else
                Dim yRange As Double = Math.Max(New Number(Of T, TCalculator)(left.Y2) - New Number(Of T, TCalculator)(left.Y1),
                                                New Number(Of T, TCalculator)(right.Y2) - New Number(Of T, TCalculator)(right.Y1))
                Return Math.Abs(right.Offset - left.Offset) < Math.Max(Single.Epsilon, tolerance * Math.Abs(yRange)) AndAlso
                       Math.Abs(right.Slope - left.Slope) < Math.Max(Single.Epsilon, tolerance * Math.Abs(Math.Max(left.Slope, right.Slope)))
            End If
        End Function

        ''' <summary>
        ''' Returns the Unity line.
        ''' </summary>
        ''' <value>
        ''' A <see cref="Line(of T, TC)">((0,0)-(1,1)) Line</see>.
        ''' </value>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Shared ReadOnly Property Unity() As Line(Of T, TCalculator)
            Get
                Dim a As New Number(Of T, TCalculator)(0)
                Dim b As New Number(Of T, TCalculator)(1)
                Return New Line(Of T, TCalculator)(a.Value, a.Value, b.Value, b.Value)
            End Get
        End Property

        ''' <summary>
        ''' Returns the Zero line.
        ''' </summary>
        ''' <value>
        ''' A <see cref="Line(of T, TC)">((0,0)-(0,0)) Line</see>.
        ''' </value>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Shared ReadOnly Property Zero() As Line(Of T, TCalculator)
            Get
                Dim a As New Number(Of T, TCalculator)(0)
                Return New Line(Of T, TCalculator)(a, a, a, a)
            End Get
        End Property

#End Region

#Region " METHODS and PROPERTIES "

        ''' <summary>
        ''' Returns true if the line equals to the line instance.
        ''' </summary>
        ''' <param name="lineCompared"></param>
        ''' <param name="tolerance"></param>
        Public Overloads Function Equals(ByVal lineCompared As Line(Of T, TCalculator), ByVal tolerance As Double) As Boolean
            If lineCompared Is Nothing Then
                Return False
            Else
                Return Line(Of T, TCalculator).Equals(Me, lineCompared, tolerance)
            End If
        End Function

        ''' <summary>
        ''' Returns the Maximum Norm of the line.
        ''' </summary>
        ''' <remarks>The maximum norm equals the max1imum absolute value of the two coordinates.
        ''' of the line.
        ''' </remarks>
        Public ReadOnly Property MaximumNorm() As T
            Get
                Return Number(Of T, TCalculator).MaximumNorm(Number(Of T, TCalculator).MaximumNorm(MyBase.Y1, MyBase.X1), Number(Of T, TCalculator).MaximumNorm(MyBase.Y2, MyBase.X2))
            End Get
        End Property

        Private _offset As Double
        ''' <summary>Gets or sets the Offset of the line y = slope * x + offset.
        '''   This is the Y value at x = 0.</summary>
        ''' <value>A <see cref="System.Int32">Int32</see> property</value>
        Public ReadOnly Property Offset() As Double
            Get
                Return Me._offset
            End Get
        End Property

        ''' <summary>Sets the line based on the coordinates.</summary>
        ''' <param name="x1">Specifies the X1 coordinate of the line.</param>
        ''' <param name="y1">Specifies the Y1 coordinate of the line.</param>
        ''' <param name="x2">Specifies the X2 coordinate of the line.</param>
        ''' <param name="y2">Specifies the Y2 coordinate of the line.</param>
        ''' <remarks>Use this class to set the line.</remarks>
        Public Overrides Sub SetLine(ByVal x1 As T, ByVal y1 As T,
                                     ByVal x2 As T, ByVal y2 As T)

            MyBase.SetLine(x1, y1, x2, y2)
            Me.UpdateSlopeOffset()

        End Sub

        ''' <summary>
        ''' Sets the Line based on the slope and offset values.
        ''' </summary>
        ''' <param name="slope">Specifies the slope of the line.</param>
        ''' <param name="offset">A <see cref="System.Double">Double</see> expression that specifies the offset
        '''   of the line.</param>
        Public Overloads Sub SetLine(ByVal slope As Double, ByVal offset As Double)

            Me.SetLine(New Number(Of T, TCalculator), New Number(Of T, TCalculator) + offset, New Number(Of T, TCalculator) + 1, New Number(Of T, TCalculator) + slope + offset)

        End Sub

        Private _slope As Double
        ''' <summary>Gets or sets the Slope of the line y = slope * x + offset.</summary>
        ''' <value>A <see cref="System.Int32">Int32</see> property</value>
        Public ReadOnly Property Slope() As Double
            Get
                Return Me._slope
            End Get
        End Property

        ''' <summary>
        ''' Recalculate offset and slope.
        ''' </summary>
        Protected Sub UpdateSlopeOffset()
            Me._slope = Line(Of T, TCalculator).ComputeSlope(Me)
            Me._offset = Line(Of T, TCalculator).ComputeOffset(Me)
        End Sub

        ''' <summary>Returns the X line value for the give Y value.</summary>
        ''' <param name="yValue">Specifies the Y value for which to get the line value.</param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="X")>
        Public Function X(ByVal yValue As T) As T
            If Me.Slope <> 0 Then
                Return CType((New Number(Of T, TCalculator)(yValue) - Me.Offset) / Me.Slope, Number(Of T, TCalculator))
            Else
                Return CType(Me.Offset, Number(Of T, TCalculator))
            End If
        End Function

        ''' <summary>
        ''' Holds the X1 coordinate of the line.
        ''' </summary>
        Public Overrides Property X1() As T
            Get
                Return MyBase.X1
            End Get
            Set(ByVal Value As T)
                If Value.CompareTo(MyBase.X1) <> 0 Then
                    MyBase.X1 = Value
                    If Not MyBase.SuspendUpdate Then
                        Me.UpdateSlopeOffset()
                    End If
                End If
            End Set
        End Property

        ''' <summary>
        ''' Holds the X2 coordinate of the line.
        ''' </summary>
        Public Overrides Property X2() As T
            Get
                Return MyBase.X2
            End Get
            Set(ByVal Value As T)
                If Value.CompareTo(MyBase.X2) <> 0 Then
                    MyBase.X2 = Value
                    If Not MyBase.SuspendUpdate Then
                        Me.UpdateSlopeOffset()
                    End If
                End If
            End Set
        End Property

        ''' <summary>
        ''' Holds the Y1 coordinate of the line.
        ''' </summary>
        Public Overrides Property Y1() As T
            Get
                Return MyBase.Y1
            End Get
            Set(ByVal Value As T)
                If Value.CompareTo(MyBase.Y1) <> 0 Then
                    MyBase.Y1 = Value
                    If Not MyBase.SuspendUpdate Then
                        Me.UpdateSlopeOffset()
                    End If
                End If
            End Set
        End Property

        ''' <summary>
        ''' Holds the Y2 coordinate of the line.
        ''' </summary>
        Public Overrides Property Y2() As T
            Get
                Return MyBase.Y2
            End Get
            Set(ByVal Value As T)
                If Value.CompareTo(MyBase.Y2) <> 0 Then
                    MyBase.Y2 = Value
                    If Not MyBase.SuspendUpdate Then
                        Me.UpdateSlopeOffset()
                    End If
                End If
            End Set
        End Property

        ''' <summary>Returns the Y line value for the give X value.</summary>
        ''' <param name="x">Specifies the X value for which to get the line value.</param>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Y")>
        Public Function Y(ByVal x As T) As T
            Return CType(New Number(Of T, TCalculator)(x) * Me.Slope + Me.Offset, Number(Of T, TCalculator))
        End Function

#End Region

    End Class

    ''' <summary>
    ''' Implements a generic line class as a line between two generic <see cref="Point(Of T, TCalculator)">points</see>.
    ''' </summary>
    ''' <typeparam name="T">Specifies the generic type of the instance of the class.</typeparam>
    Public Class Cord(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable}, TCalculator As {ICalculator(Of T), New})

        Inherits Line(Of T, TCalculator)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>
        ''' Constructs a <see cref="Line(of T)"/> instance by its origin and insertion points.
        ''' </summary>
        ''' <param name="origin">Specifies the <see cref="Point(of T)">origin</see> or start point of the line.</param>
        ''' <param name="insertion">Specifies the <see cref="Point(of T)">insertion</see> or end point of the line.</param>
        Public Sub New(ByVal origin As Point(Of T), ByVal insertion As Point(Of T))

            MyBase.new()
            If origin IsNot Nothing Then
                Me.Origin = New Point(Of T)(origin)
            End If
            If insertion IsNot Nothing Then
                Me.Insertion = New Point(Of T)(insertion)
            End If

        End Sub

#End Region

#Region " METHODS AND PROPERTIES "

        ''' <summary>Gets or sets the insertion point of the line.</summary>
        ''' <value>A <see cref="Point(of T)">Point</see> property</value>
        Public Property Insertion() As Point(Of T)
            Get
                Return New Point(Of T)(MyBase.X2, MyBase.Y2)
            End Get
            Set(ByVal Value As Point(Of T))
                If Value IsNot Nothing Then
                    If MyBase.SuspendUpdate Then
                        MyBase.X2 = Value.X
                        MyBase.Y2 = Value.Y
                    Else
                        MyBase.SuspendUpdate = True
                        MyBase.X2 = Value.X
                        MyBase.SuspendUpdate = False
                        MyBase.Y2 = Value.Y
                    End If
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the origin point of the line.</summary>
        ''' <value>A <see cref="Point(of T)">Point</see> property</value>
        Public Property Origin() As Point(Of T)
            Get
                Return New Point(Of T)(MyBase.X1, MyBase.Y1)
            End Get
            Set(ByVal Value As Point(Of T))
                If Value IsNot Nothing Then
                    If MyBase.SuspendUpdate Then
                        MyBase.X1 = Value.X
                        MyBase.Y1 = Value.Y
                    Else
                        MyBase.SuspendUpdate = True
                        MyBase.X1 = Value.X
                        MyBase.SuspendUpdate = False
                        MyBase.Y1 = Value.Y
                    End If
                End If
            End Set
        End Property

        ''' <summary>
        ''' Sets the line coordinates.
        ''' </summary>
        ''' <param name="origin">Specifies the origin point of the line.</param>
        ''' <param name="insertion">Specifies the insertion point of the line.</param>
        Public Overloads Sub SetLine(ByVal origin As IPoint(Of T), ByVal insertion As IPoint(Of T))

            If origin Is Nothing Then
                Throw New ArgumentNullException("origin")
            End If
            If insertion Is Nothing Then
                Throw New ArgumentNullException("insertion")
            End If
            MyBase.SetLine(origin.X, origin.Y, insertion.X, insertion.Y)

        End Sub

#End Region

    End Class


End Namespace

