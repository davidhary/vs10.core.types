﻿Namespace Calculable

    ''' <summary>
    ''' Implements a generic point class capable of internal calculations such as 
    ''' the maximum norm.  This could be extended to provide additional capabilities.
    ''' </summary>
    ''' <typeparam name="T">Specifies the type parameter of the generic class.</typeparam>
    ''' <typeparam name="TCalculator">Specifies the calculator structure that applies
    ''' to the type parameter.</typeparam>
    Public Class Point(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable}, TCalculator As {ICalculator(Of T), New})

        Inherits isr.Core.Types.Point(Of T)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>The Copy Constructor</summary>
        ''' <param name="model">The <see cref="Point">Point</see> object from which to copy</param>
        Public Sub New(ByVal model As Point(Of T, TCalculator))
            MyBase.New()
            If model IsNot Nothing Then
                Me.SetPoint(model.X, model.Y)
            End If
        End Sub

        ''' <summary>
        ''' Constructs this class.
        ''' </summary>
        ''' <param name="x">Specifies the X coordinate of the point.</param>
        ''' <param name="y">Specifies the Y coordinate of the point.</param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
        Public Sub New(ByVal x As T, ByVal y As T)
            MyBase.new(x, y)
        End Sub

#End Region

#Region " SHARED "

        ''' <summary>
        ''' Return the Unity [1,1] point.
        ''' </summary>
        ''' <value>
        ''' A <see cref="Point(of T, C)">[1,1] Point</see>.
        ''' </value>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Shared ReadOnly Property Unity() As Point(Of T, TCalculator)
            Get
                Return New Point(Of T, TCalculator)(New Number(Of T, TCalculator)(1), New Number(Of T, TCalculator)(1))
            End Get
        End Property

        ''' <summary>
        ''' Returns the Zero [0,0] point.
        ''' </summary>
        ''' <value>
        ''' A <see cref="Point(of T, C)">[0,0] Point</see>.
        ''' </value>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Shared ReadOnly Property Zero() As Point(Of T, TCalculator)
            Get
                Return New Point(Of T, TCalculator)(New Number(Of T, TCalculator)(0), New Number(Of T, TCalculator)(0))
            End Get
        End Property

#End Region

#Region " METHODS and PROPERTIES "

        ''' <summary>
        ''' Returns the Maximum Norm of the point.
        ''' </summary>
        ''' <remarks>The maximum norm equals the maximum absolute value of the two coordinates.
        ''' of the point.
        ''' </remarks>
        Public ReadOnly Property MaximumNorm() As T
            Get
                Return Number(Of T, TCalculator).MaximumNorm(MyBase.Y, MyBase.X)
            End Get
        End Property

#End Region

    End Class

End Namespace
