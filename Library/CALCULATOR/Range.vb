﻿Namespace Calculable

    ''' <summary>
    ''' Implements a generic range class capable of returning the span and mid point
    ''' of the range.
    ''' </summary>
    ''' <typeparam name="T">Specifies the type parameter of the generic class.</typeparam>
    ''' <typeparam name="TCalculator">The type of the T calculator.</typeparam>
    Public Class Range(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable}, TCalculator As {ICalculator(Of T), New})

        Inherits isr.Core.Types.Range(Of T)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>
        ''' Initializes a new instance of the <see cref="Range" /> class.
        ''' The copy constructor.
        ''' </summary>
        ''' <param name="model">The <see cref="Range">Range</see> object from which to copy</param>
        Public Sub New(ByVal model As Range(Of T, TCalculator))
            MyBase.New()
            If model IsNot Nothing Then
                Me.SetRange(model.Min, model.Max)
            End If
        End Sub

        ''' <summary>
        ''' Constructs this class.
        ''' </summary>
        ''' <param name="minValue">Min of range</param>
        ''' <param name="maxValue">End of range</param>
        Public Sub New(ByVal minValue As T, ByVal maxValue As T)

            MyBase.new(minValue, maxValue)

        End Sub

#End Region

#Region " SHARED "

        ''' <summary>
        ''' Returns the empty [0,0] range.
        ''' </summary>
        ''' <value>
        ''' A <see cref="Range(of T, TC)"/> value Zero minimum and maximum values.
        ''' </value>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Shared ReadOnly Property [Empty]() As Range(Of T, TCalculator)
            Get
                Dim a As New Number(Of T, TCalculator)(0)
                Return New Range(Of T, TCalculator)(a, a)
            End Get
        End Property

        ''' <summary>
        ''' Return the Unity [0,1] range.
        ''' </summary>
        ''' <value>A <see cref="Range(of T, TC)">[0,1] Range</see>.</value>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Shared ReadOnly Property Unity() As Range(Of T, TCalculator)
            Get
                Return New Range(Of T, TCalculator)(New Number(Of T, TCalculator)(0), New Number(Of T, TCalculator)(1))
            End Get
        End Property

        ''' <summary>
        ''' Returns the Zero [0,0] range.
        ''' </summary>
        ''' <value>
        ''' A <see cref="Range(of T, TC)"/> value Zero minimum and maximum values.
        ''' </value>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Shared ReadOnly Property Zero() As Range(Of T, TCalculator)
            Get
                Dim a As New Number(Of T, TCalculator)(0)
                Return New Range(Of T, TCalculator)(a, a)
            End Get
        End Property

        ''' <summary>
        ''' Compares two ranges. The ranges are compared using their slopes and offsets.</summary>
        ''' <param name="left">Specifies the <see cref="Range(of T, TC)">Range</see>
        '''   to compare with.</param>
        ''' <param name="right">Specifies the <see cref="Range(of T, TC)">Range</see>
        '''   to compare to.</param>
        ''' <param name="tolerance">Specifies the relative tolerance for comparing the two
        '''   ranges.  The ranges are compared based on their slope and offset.  The offset
        '''   tolerance is based on it relative change from the Y range.  The tolerance if based on
        '''   the reference range.</param>
        ''' <returns>A Boolean data type</returns>
        ''' <remarks>The two ranges are the same if the have the same minimum and maximum
        '''   values.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Overloads Shared Function Equals(ByVal left As Range(Of T), ByVal right As Range(Of T), ByVal tolerance As Double) As Boolean
            If left Is Nothing Then
                Return right Is Nothing
            ElseIf right Is Nothing Then
                Return False
            Else
                Return Math.Abs(New Number(Of T, TCalculator)(left.Min) - New Number(Of T, TCalculator)(right.Min)) < tolerance AndAlso
                       Math.Abs(New Number(Of T, TCalculator)(left.Max) - New Number(Of T, TCalculator)(right.Max)) < tolerance
            End If
        End Function

#End Region

#Region " METHODS and PROPERTIES "

        ''' <summary>
        ''' Returns a new range extended by the specified amount.
        ''' </summary>
        ''' <param name="originalRange"></param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
        Public Overloads Shared Function Extend(ByVal originalRange As Range(Of T), ByVal value As T) As Range(Of T, TCalculator)

            If originalRange Is Nothing Then
                Throw New ArgumentNullException("originalRange")
            End If
            Return New Range(Of T, TCalculator)(New Number(Of T, TCalculator)(originalRange.Min) - value, New Number(Of T, TCalculator)(originalRange.Max) + value)
        End Function

        ''' <summary>
        ''' Extends this range by the specified <paramref name="span">amount</paramref>.
        ''' </summary>
        ''' <param name="value">Specifies the amount by which to extend the range.</param>
        Public Overloads Function ExtendBy(ByVal value As T) As Range(Of T, TCalculator)

            Me.SetRange(New Number(Of T, TCalculator)(Me.Min) - value, New Number(Of T, TCalculator)(Me.Max) + value)
            Return Me

        End Function

        ''' <summary>
        ''' Returns true if the <paramref name="value">specified value</paramref>
        ''' is within the extended range.
        ''' </summary>
        ''' <param name="value">Specifies the value which to check as contained within
        ''' the range.</param>
        ''' <param name="extendBy">Specifies the amount by which to extend the range for checking
        ''' the value.
        ''' </param>
        Public Overloads Function Contains(ByVal value As T, ByVal extendBy As T) As Boolean
            Return Range(Of T, TCalculator).Extend(Me, extendBy).Contains(value)
        End Function

        ''' <summary>
        ''' Returns the Maximum Norm of the range.
        ''' </summary>
        ''' <remarks>The maximum norm equals the maximum absolute value 
        ''' of the range.
        ''' </remarks>
        Public ReadOnly Property MaximumNorm() As T
            Get
                Return Number(Of T, TCalculator).MaximumNorm(MyBase.Max, MyBase.Min)
            End Get
        End Property

        ''' <summary>
        ''' Returns the span of the range.
        ''' </summary>
        Public ReadOnly Property Span() As T
            Get
                Dim diff As Number(Of T, TCalculator) = MyBase.Max
                diff -= MyBase.Min
                Return diff
            End Get
        End Property

        ''' <summary>
        ''' Returns the mid range point of the range.
        ''' </summary>
        Public ReadOnly Property Mid() As T
            Get
                Dim diff As Number(Of T, TCalculator) = MyBase.Max
                diff += MyBase.Min
                Return 0.5 * diff
            End Get
        End Property

#End Region

    End Class

End Namespace

