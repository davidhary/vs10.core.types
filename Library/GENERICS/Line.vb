''' <summary>
''' A Interface definition of line classes.
''' </summary>
''' <typeparam name="T">Specifies the type parameter of the generic class.</typeparam>
Public Interface ILine(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable})

    Property X1() As T
    Property X2() As T
    Property Y1() As T
    Property Y2() As T
    Function Equals(ByVal value As Object) As Boolean
    Function Equals(ByVal lineCompared As Line(Of T)) As Boolean
    Function GetHashCode() As Int32
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    Sub SetLine(ByVal x1 As T, ByVal y1 As T, ByVal x2 As T, ByVal y2 As T)
    Function ToString() As String

End Interface

''' <summary>
''' Implements a generic line class.
''' </summary>
''' <typeparam name="T">Specifies the type parameter of the generic class.</typeparam>
Public Class Line(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable})

    Implements ILine(Of T)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Line" /> class.
    ''' </summary>
    Protected Sub New()
        MyBase.New()
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Line" /> class.
    ''' </summary>
    ''' <param name="x1">Specifies the X1 coordinate of the line.</param>
    ''' <param name="y1">Specifies the Y1 coordinate of the line.</param>
    ''' <param name="x2">Specifies the X2 coordinate of the line.</param>
    ''' <param name="y2">Specifies the Y2 coordinate of the line.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    Public Sub New(ByVal x1 As T, ByVal y1 As T, ByVal x2 As T, ByVal y2 As T)
        Me.new()
        Me._SetLine(x1, y1, x2, y2)
    End Sub

    ''' <summary>The Copy Constructor</summary>
    ''' <param name="model">The  <see cref="Line">Line</see> object from which to Copy</param>
    Public Sub New(ByVal model As Line(Of T))

        Me.New()
        If model IsNot Nothing Then
            Me._SetLine(model._x1, model._y1, model._x2, model._y2)
        End If

    End Sub


#End Region

#Region " EQUALS "

    ''' <summary> Compares two lines. </summary>
    ''' <param name="left">  Specifies the line to compare to. </param>
    ''' <param name="right"> Specifies the line to compare. </param>
    ''' <returns> True if the lines are equal. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
    Public Overloads Shared Function Equals(ByVal left As Object, ByVal right As Object) As Boolean
        Return Line(Of T).Equals(TryCast(left, Line(Of T)), TryCast(right, Line(Of T)))
    End Function

    ''' <summary>
    ''' Compares two lines.
    ''' </summary>
    ''' <param name="left">Specifies the line to compare to.</param>
    ''' <param name="right">Specifies the line to compare.</param>
    ''' <returns>True if the lines are equal.</returns>
    ''' <remarks>
    ''' The two lines are the same if they have the same X and Y coordinates.
    ''' </remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
    Public Overloads Shared Function Equals(ByVal left As Line(Of T), ByVal right As Line(Of T)) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return left.X1.Equals(right.X1) AndAlso left.X2.Equals(right.X2) AndAlso
                   left.Y1.Equals(right.Y1) AndAlso left.Y2.Equals(right.Y2)
        End If
    End Function

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    ''' <see cref="T:System.Object" />. </param>
    ''' <returns> true if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, false. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean Implements ILine(Of T).Equals
        Return obj IsNot Nothing AndAlso (Object.ReferenceEquals(Me, obj) OrElse Line(Of T).Equals(Me, CType(obj, Line(Of T))))
    End Function

    ''' <summary>
    ''' Compares two lines.
    ''' </summary>
    ''' <param name="lineCompared">Specifies the line to compare.</param>
    ''' <returns>True if the lines are equal.</returns>
    ''' <remarks>
    ''' The two lines are the same if they have the same X1 and Y1 coordinates.
    ''' </remarks>
    Public Overloads Function Equals(ByVal lineCompared As Line(Of T)) As Boolean Implements ILine(Of T).Equals
        If lineCompared Is Nothing Then
            Return False
        Else
            Return lineCompared.X1.Equals(Me.X1) AndAlso lineCompared.X2.Equals(Me.X2) AndAlso
                   lineCompared.Y1.Equals(Me.Y1) AndAlso lineCompared.Y2.Equals(Me.Y2)
        End If
    End Function

    ''' <summary>
    ''' Implements the operator =.
    ''' </summary>
    ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
    ''' <param name="right">Specifies the right hand side argument of the binary operation.</param><returns>
    ''' The result of the operator.
    ''' </returns>
    Public Shared Operator =(ByVal left As Line(Of T), ByVal right As Line(Of T)) As Boolean
        Return ((left Is Nothing) AndAlso (right Is Nothing)) OrElse (left IsNot Nothing) AndAlso left.Equals(right)
    End Operator

    ''' <summary>
    ''' Implements the operator &lt;&gt;.
    ''' </summary>
    ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
    ''' <param name="right">Specifies the right hand side argument of the binary operation.</param><returns>
    ''' The result of the operator.
    ''' </returns>
    Public Shared Operator <>(ByVal left As Line(Of T), ByVal right As Line(Of T)) As Boolean
        Return ((left Is Nothing) AndAlso (right IsNot Nothing)) OrElse Not ((left IsNot Nothing) AndAlso left.Equals(right))
    End Operator

    ''' <summary>
    ''' Creates a unique hash code.
    ''' </summary>
    ''' <returns>An <see cref="System.Int32">Int32</see> value</returns>
    Public Overloads Overrides Function GetHashCode() As Int32 Implements ILine(Of T).GetHashCode
        Return Me.X1.GetHashCode Xor Me.Y1.GetHashCode Xor Me.X2.GetHashCode Xor Me.Y2.GetHashCode
    End Function

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary>Sets the line based on the coordinates.</summary>
    ''' <param name="x1">Specifies the X1 coordinate of the line.</param>
    ''' <param name="y1">Specifies the Y1 coordinate of the line.</param>
    ''' <param name="x2">Specifies the X2 coordinate of the line.</param>
    ''' <param name="y2">Specifies the Y2 coordinate of the line.</param>
    ''' <remarks>Use this class to set the line.</remarks>
    Private Sub _SetLine(ByVal x1 As T, ByVal y1 As T, ByVal x2 As T, ByVal y2 As T)
        Me._x1 = x1
        Me._y1 = y1
        Me._x2 = x2
        Me._y2 = y2
    End Sub

    ''' <summary>Sets the line based on the coordinates.</summary>
    ''' <param name="x1">Specifies the X1 coordinate of the line.</param>
    ''' <param name="y1">Specifies the Y1 coordinate of the line.</param>
    ''' <param name="x2">Specifies the X2 coordinate of the line.</param>
    ''' <param name="y2">Specifies the Y2 coordinate of the line.</param>
    ''' <remarks>Use this class to set the line.</remarks>
    Public Overridable Sub SetLine(ByVal x1 As T, ByVal y1 As T, ByVal x2 As T, ByVal y2 As T) Implements ILine(Of T).SetLine
        Me._SetLine(x1, y1, x2, y2)
    End Sub

    ''' <summary>
    ''' Returns the default string representation of the line.
    ''' </summary>
    Public Overrides Function ToString() As String Implements ILine(Of T).ToString
        Return Line(Of T).ToString(Me.X1, Me.Y1, Me.X2, Me.Y2)
    End Function

    ''' <summary>Returns the default string representation of the line.</summary>
    Private Overloads Shared Function ToString(ByVal x1 As T, ByVal y1 As T, ByVal x2 As T, ByVal y2 As T) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "[({0},{1})-({2},{3})]", x1, y1, x2, y2)
    End Function

    ''' <summary>
    ''' Transposes the (x,y) line to a (y,x) line.
    ''' </summary>
    ''' <param name="line">
    ''' Specifies the <see cref="Line">Line</see> to transpose.
    ''' </param>
    Private Shared Function Transpose(ByVal line As Line(Of T)) As Line(Of T)

        If line Is Nothing Then
            Throw New ArgumentNullException("line")
        End If
        Return New Line(Of T)(line.Y1, line.X1, line.Y2, line.X2)

    End Function

    ''' <summary>
    ''' Holds the X1 coordinate of the line.
    ''' </summary>
    Public Overridable Property X1() As T Implements ILine(Of T).X1

    ''' <summary>
    ''' Holds the X2 coordinate of the line.
    ''' </summary>
    Public Overridable Property X2() As T Implements ILine(Of T).X2

    ''' <summary>
    ''' Holds the Y1 coordinate of the line.
    ''' </summary>
    Public Overridable Property Y1() As T Implements ILine(Of T).Y1

    ''' <summary>
    ''' Holds the Y2 coordinate of the line.
    ''' </summary>
    Public Overridable Property Y2() As T Implements ILine(Of T).Y2

    ''' <summary>
    ''' Suspends update of changes.
    ''' </summary>
    Protected Property SuspendUpdate() As Boolean

#End Region

End Class

''' <summary>
''' Implements a generic line class as a line between two generic <see cref="Point(Of T)">points</see>.
''' </summary>
''' <typeparam name="T">Specifies the generic type of the instance of the class.</typeparam>
Public Class Cord(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable})

    Inherits Line(Of T)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs a <see cref="Line(of T)"/> instance by its origin and insertion points.
    ''' </summary>
    ''' <param name="origin">Specifies the <see cref="Point(of T)">origin</see> or start point of the line.</param>
    ''' <param name="insertion">Specifies the <see cref="Point(of T)">insertion</see> or end point of the line.</param>
    Public Sub New(ByVal origin As Point(Of T), ByVal insertion As Point(Of T))

        MyBase.new()
        Me.SetLine(origin, insertion)
    End Sub

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary>Gets or sets the insertion point of the line.</summary>
    ''' <value>A <see cref="Point(of T)">Point</see> property</value>
    Public Property Insertion() As Point(Of T)
        Get
            Return New Point(Of T)(MyBase.X2, MyBase.Y2)
        End Get
        Set(ByVal Value As Point(Of T))
            If Value IsNot Nothing Then
                MyBase.X2 = Value.X
                MyBase.Y2 = Value.Y
            End If
        End Set
    End Property

    ''' <summary>Gets or sets the origin point of the line.</summary>
    ''' <value>A <see cref="Point(of T)">Point</see> property</value>
    Public Property Origin() As Point(Of T)
        Get
            Return New Point(Of T)(MyBase.X1, MyBase.Y1)
        End Get
        Set(ByVal Value As Point(Of T))
            If Value IsNot Nothing Then
                MyBase.X1 = Value.X
                MyBase.Y1 = Value.Y
            End If
        End Set
    End Property

    ''' <summary>
    ''' Sets the line coordinates.
    ''' </summary>
    ''' <param name="origin">Specifies the origin point of the line.</param>
    ''' <param name="insertion">Specifies the insertion point of the line.</param>
    Public Overloads Sub SetLine(ByVal origin As Point(Of T), ByVal insertion As Point(Of T))

        If origin Is Nothing Then
            Throw New ArgumentNullException("origin")
        End If
        If insertion Is Nothing Then
            Throw New ArgumentNullException("insertion")
        End If
        Me.Origin = New Point(Of T)(origin)
        Me.Insertion = New Point(Of T)(insertion)

    End Sub

#End Region

End Class
