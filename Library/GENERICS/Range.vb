''' <summary>
''' A Interface definition of range classes.
''' </summary>
''' <typeparam name="T">Specifies the type parameter of the generic class.</typeparam>
Public Interface IRange(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable})

    ReadOnly Property Min() As T
    ReadOnly Property Max() As T
    Function Contains(ByVal value As T) As Boolean
    Function Equals(ByVal value As Object) As Boolean
    Function Equals(ByVal rangeCompared As Range(Of T)) As Boolean
    Function ExtendBy(ByVal range As Range(Of T)) As Range(Of T)
    Function GetHashCode() As Int32
    Sub SetRange(ByVal minValue As T, ByVal maxValue As T)
    Function ToString() As String

End Interface

''' <summary>
''' Implements a generic range class.
''' </summary>
''' <typeparam name="T">Specifies the type parameter of the generic class.</typeparam>
Public Class Range(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable})

    Implements IRange(Of T)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Range" /> class.
    ''' </summary>
    Protected Sub New()
        MyBase.New()
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Range" /> class.
    ''' The copy constructor.
    ''' </summary>
    ''' <param name="model">The  <see cref="Range">Range</see> object from which to copy</param>
    Public Sub New(ByVal model As Range(Of T))
        MyBase.New()
        If model IsNot Nothing Then
            Me._SetRange(model._min, model._max)
        End If
    End Sub

    ''' <summary>
    ''' Constructs this class.
    ''' </summary>
    ''' <param name="minValue">Min of range</param>
    ''' <param name="maxValue">End of range</param>
    Public Sub New(ByVal minValue As T, ByVal maxValue As T)
        MyBase.New()
        Me._SetRange(minValue, maxValue)
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> true if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, false. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
    Public Overloads Shared Function Equals(ByVal left As Object, ByVal right As Object) As Boolean
        Return Range(Of T).Equals(TryCast(left, Range(Of T)), TryCast(right, Range(Of T)))
    End Function

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> true if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, false. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
    Public Overloads Shared Function Equals(ByVal left As Range(Of T), ByVal right As Range(Of T)) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return left.Min.Equals(right.Min) AndAlso left.Max.Equals(right.Max)
        End If
    End Function

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    ''' <see cref="T:System.Object" />. </param>
    ''' <returns> true if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, false. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean Implements IRange(Of T).Equals
        Return obj IsNot Nothing AndAlso (Object.ReferenceEquals(Me, obj) OrElse Range(Of T).Equals(Me, CType(obj, Range(Of T))))
    End Function

    ''' <summary>
    ''' Compares two ranges.
    ''' </summary>
    ''' <param name="rangeCompared">Specifies the range to compare.</param>
    ''' <returns>True if the ranges are equal.</returns>
    ''' <remarks>
    ''' The two ranges are the same if they have the min and end values.
    ''' </remarks>
    Public Overloads Function Equals(ByVal rangeCompared As Range(Of T)) As Boolean Implements IRange(Of T).Equals
        If rangeCompared Is Nothing Then
            Return False
        Else
            Return Range(Of T).Equals(Me, rangeCompared)
        End If
    End Function

    ''' <summary>
    ''' Creates a unique hash code.
    ''' </summary>
    ''' <returns>An <see cref="System.Int32">Int32</see> value</returns>
    Public Overloads Overrides Function GetHashCode() As Int32 Implements IRange(Of T).GetHashCode
        Return Me.Min.GetHashCode Xor Me.Max.GetHashCode
    End Function

#End Region

#Region " OPERATORS "

    ''' <summary>
    ''' Implements the operator =.
    ''' </summary>
    ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
    ''' <param name="right">Specifies the right hand side argument of the binary operation.</param><returns>
    ''' The result of the operator.
    ''' </returns>
    Public Shared Operator =(ByVal left As Range(Of T), ByVal right As Range(Of T)) As Boolean
        Return ((left Is Nothing) AndAlso (right Is Nothing)) OrElse (left IsNot Nothing) AndAlso left.Equals(right)
    End Operator

    ''' <summary>
    ''' Implements the operator &lt;&gt;.
    ''' </summary>
    ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
    ''' <param name="right">Specifies the right hand side argument of the binary operation.</param><returns>
    ''' The result of the operator.
    ''' </returns>
    Public Shared Operator <>(ByVal left As Range(Of T), ByVal right As Range(Of T)) As Boolean
        Return ((left Is Nothing) AndAlso (right IsNot Nothing)) OrElse Not ((left IsNot Nothing) AndAlso left.Equals(right))
    End Operator

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary>
    ''' Returns true if the <paramref name="value">specified value</paramref>
    ''' is within range.
    ''' </summary>
    ''' <param name="value">Specifies the value which to check as contained within
    ''' the range.</param>
    Public Function Contains(ByVal value As T) As Boolean Implements IRange(Of T).Contains
        Return value.CompareTo(Me.Min) >= 0 AndAlso value.CompareTo(Me.Max) <= 0
    End Function

    ''' <summary>
    ''' Returns a new range from the min of the two minima to the max of the two maxima.</summary>
    ''' <param name="rangeA">Specifies <see cref="Range(of T)"/> A.</param>
    ''' <param name="rangeB">Specifies <see cref="Range(of T)"/> B.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
    Public Shared Function Extend(ByVal rangeA As Range(Of T), ByVal rangeB As Range(Of T)) As Range(Of T)

        If rangeA Is Nothing Then
            Throw New ArgumentNullException("rangeA")
        End If

        If rangeB Is Nothing Then
            Throw New ArgumentNullException("rangeB")
        End If

        If rangeA.Min.CompareTo(rangeB.Min) > 0 Then
            If rangeA.Max.CompareTo(rangeB.Max) < 0 Then
                Return New Range(Of T)(rangeB.Min, rangeB.Max)
            Else
                Return New Range(Of T)(rangeB.Min, rangeA.Max)
            End If
        ElseIf rangeA.Max.CompareTo(rangeB.Max) < 0 Then
            Return New Range(Of T)(rangeA.Min, rangeB.Max)
        Else
            Return New Range(Of T)(rangeA.Min, rangeA.Max)
        End If

    End Function

    ''' <summary>
    ''' Extends this range to include both its present values and the
    ''' specified range.</summary>
    ''' <param name="range">A <see cref="Range(of T)"/> value</param>
    Public Function ExtendBy(ByVal range As Range(Of T)) As Range(Of T) Implements IRange(Of T).ExtendBy

        If range Is Nothing Then
            Throw New ArgumentNullException("range")
        End If

        If Me.Min.CompareTo(range.Min) > 0 Then
            Me.SetRange(range.Min, Me.Min)
        End If

        If Me.Max.CompareTo(range.Max) < 0 Then
            Me.SetRange(Me.Min, range.Max)
        End If

        Return Me

    End Function

    ''' <summary>Return the range of the specified data array.</summary>
    ''' <param name="values">The data array</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
    Public Shared Function GetRange(ByVal values() As T) As Range(Of T)

        ' return the unit range if no data
        If values Is Nothing Then
            Throw New ArgumentNullException("values")
        End If

        ' initialize the range values to the first value
        Dim temp As T
        temp = values(0)
        Dim min As T = temp
        Dim max As T = temp

        ' Loop over each point in the arrays
        For i As Int32 = 0 To values.Length - 1
            temp = values(i)
            If temp.CompareTo(min) < 0 Then
                min = temp
            ElseIf temp.CompareTo(max) > 0 Then
                max = temp
            End If
        Next i
        Return New Range(Of T)(min, max)

    End Function

    Private _max As T
    ''' <summary>
    ''' Returns the end or maximum value of the range.
    ''' </summary>
    Public ReadOnly Property Max() As T Implements IRange(Of T).Max
        Get
            Return Me._max
        End Get
    End Property

    Private _min As T
    ''' <summary>
    ''' Returns the start or minimum value of the range.
    ''' </summary>
    Public ReadOnly Property Min() As T Implements IRange(Of T).Min
        Get
            Return Me._min
        End Get
    End Property

    ''' <summary>Sets the range based on the extrema.</summary>
    ''' <param name="minValue">Specified the minimum value of the range.</param>
    ''' <param name="maxValue">Specifies the maximum value of the range.</param>
    ''' <remarks>Use this class to set the range.</remarks>
    Private Sub _SetRange(ByVal minValue As T, ByVal maxValue As T)

        If minValue.CompareTo(maxValue) <= 0 Then
            Me._min = minValue
            Me._max = maxValue
        Else
            Me._min = maxValue
            Me._max = minValue
        End If

    End Sub

    ''' <summary>Sets the range based on the extrema.</summary>
    ''' <param name="minValue">Specified the minimum value of the range.</param>
    ''' <param name="maxValue">Specifies the maximum value of the range.</param>
    ''' <remarks>Use this class to set the range.</remarks>
    Public Sub SetRange(ByVal minValue As T, ByVal maxValue As T) Implements IRange(Of T).SetRange
        Me._SetRange(minValue, maxValue)
    End Sub

    ''' <summary>
    ''' Returns the default string representation of the range.
    ''' </summary>
    Public Overrides Function ToString() As String Implements IRange(Of T).ToString
        Return Range(Of T).ToString(Me.Min, Me.Max)
    End Function

    ''' <summary>Returns the default string representation of the range.</summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
    Public Overloads Shared Function ToString(ByVal min As T, ByVal max As T) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture,
                             "({0},{1})",
                             min, max)
    End Function

#End Region

End Class
