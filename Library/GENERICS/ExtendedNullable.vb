''' <summary>
''' Extends the .NET nullable Double.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="11/14/2007" by="David" revision="1.1.2874.x">
''' Created
''' </history>
<CLSCompliant(False)>
Public Class ExtendedNullable(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable, IConvertible})

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ExtendedNullable" /> class.
    ''' </summary>
    Public Sub New()

        MyBase.New()

        Me._FormatString = "0.000E00"
        Me._hasValue = False

        ' set default length to zero disabling validation of length
        Me._MaxLength = 0
        Me._MinLength = 0

    End Sub

    ''' <summary>
    ''' Initializes a new copy of the <see cref="ExtendedNullable" /> class.
    ''' </summary>
    ''' <param name="value">The value.</param>
    Public Sub New(ByVal value As ExtendedNullable(Of T))
        Me.New()
        If value IsNot Nothing Then
            Me._FormatString = value._FormatString
            Me._hasValue = value._hasValue
            Me._HighLimit = value._HighLimit
            Me._InvalidValue = value._InvalidValue
            Me._isHigh = value._isHigh
            Me._isLow = value._isLow
            Me._IsMissing = value._IsMissing
            Me._LowLimit = value._LowLimit
            Me._MaxLength = value._MaxLength
            Me._MinLength = value._MinLength
            Me._MissingValue = value._MissingValue
            Me._value = value._value
        End If
    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Gets the field value.
    ''' </summary>
    Public ReadOnly Property FieldValue() As String
        Get
            Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0:" & Me.FormatString & "}", Me.Value)
        End Get
    End Property

    ''' <summary>Gets or sets the format string for returning the field value.
    ''' </summary>
    Public Property FormatString() As String

    ''' <summary>Gets or sets the high limit.
    ''' </summary>
    Public Property HighLimit() As T

    ''' <summary>Gets or sets the instance name.</summary>
    Public Property InstanceName() As String

    ''' <summary>Gets or sets the invalid value.
    ''' </summary>
    Public Property InvalidValue() As T

    Private _hasValue As Boolean
    ''' <summary>Gets the condition for determining if a value was set.
    ''' </summary>
    Public ReadOnly Property HasValue() As Boolean
        Get
            Return Me._hasValue
        End Get
    End Property

    Private _isHigh As Boolean
    ''' <summary>
    ''' Gets the condition telling if the value is lower than the <see cref="HighLimit">high limit</see>.
    ''' </summary>
    Public ReadOnly Property IsHigh() As Boolean
        Get
            Return Me._isHigh
        End Get
    End Property

    Private _isLow As Boolean
    ''' <summary>
    ''' Gets the condition telling if the value is lower than the <see cref="LowLimit">low limit</see>.
    ''' </summary>
    Public ReadOnly Property IsLow() As Boolean
        Get
            Return Me._isLow
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance value is missing.
    ''' </summary>
    ''' <value><c>True</c> if this instance is missing; otherwise, <c>False</c>.</value>
    Public Property IsMissing() As Boolean

    ''' <summary>Gets or sets the low limit.
    ''' </summary>
    Public Property LowLimit() As T

    ''' <summary>Gets or sets the maximum length of the field value.
    ''' </summary>
    Public Property MaxLength() As Integer

    ''' <summary>Gets or sets the Minimum length of the field value.
    ''' </summary>
    Public Property MinLength() As Integer

    ''' <summary>Gets or sets the missing value.
    ''' </summary>
    Public Property MissingValue() As T

    Private _value As T
    ''' <summary>Gets or sets the value.
    ''' </summary>
    Public Property Value() As T
        Get
            If Me.IsMissing Then
                Return Me.MissingValue
            ElseIf Me.HasValue Then
                Return Me._value
            Else
                Return Me.InvalidValue
            End If
        End Get
        Set(ByVal value As T)
            Me._value = value
            Me._hasValue = True
            Me._isLow = False
            Me._isHigh = False
            If Me._value.CompareTo(Me.LowLimit) < 0 Then
                Me._isLow = True
            ElseIf Me._value.CompareTo(Me.HighLimit) > 0 Then
                Me._isHigh = True
            End If
        End Set
    End Property

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Sets the class value returning True if value was set.
    ''' </summary>
    ''' <param name="value">The value which to set.</param>
    Public Overridable Function TrySetValue(ByVal value As Double) As BooleanOutcome

        Dim tc As System.ComponentModel.TypeConverter = System.ComponentModel.TypeDescriptor.GetConverter(value.GetType())
        If tc Is Nothing Then
            Dim synopsis As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                   "{0} failed setting value.", Me.InstanceName)
            Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                  "{0} failed creating a type converter for the value='{1}'.", Me.InstanceName, value)
            Dim hint As String = "I am clueless"
            Return New BooleanOutcome(False, synopsis, details, hint)
        ElseIf Not tc.CanConvertTo(GetType(T)) Then
            Dim synopsis As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                   "{0} failed setting value.", Me.InstanceName)
            Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                  "{0} cannot convert the value='{1}' to '{2}' type.", Me.InstanceName, value, GetType(T))
            Dim hint As String = "Most likely, this method failed converting a Double type to the desired type. Please report to the developer so that the new type could be added explicitly to the conversion scheme."
            Return New BooleanOutcome(False, synopsis, details, hint)
        Else
            Me.Value = CType(tc.ConvertTo(value, GetType(T)), T)
            Return New BooleanOutcome(True)
        End If

    End Function

    ''' <summary>
    ''' Sets the class value returning True if value was set.
    ''' </summary>
    ''' <param name="value">The value which to set.</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="X")>
    Public Overridable Function TrySetValue(Of TX)(ByVal value As TX) As BooleanOutcome

        Dim tc As System.ComponentModel.TypeConverter = System.ComponentModel.TypeDescriptor.GetConverter(GetType(TX))
        If tc Is Nothing Then
            Dim synopsis As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                   "{0} failed setting value.", Me.InstanceName)
            Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                  "{0} failed creating a type converter for the value='{1}'.", Me.InstanceName, value)
            Dim hint As String = "I am clueless"
            Return New BooleanOutcome(False, synopsis, details, hint)
        ElseIf Not tc.CanConvertTo(GetType(T)) Then
            Dim synopsis As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                   "{0} failed setting value.", Me.InstanceName)
            Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                  "{0} cannot convert the value='{1}' to '{2}' type.", Me.InstanceName, value, GetType(T))
            Dim hint As String = "Most likely, this method failed converting a Double type to the desired type.  Please report to the developer so that the new type could be added explicitly to the conversion scheme."
            Return New BooleanOutcome(False, synopsis, details, hint)
        Else
            Me.Value = CType(tc.ConvertTo(value, GetType(T)), T)
            Return New BooleanOutcome(True)
        End If

    End Function

    ''' <summary>
    ''' Sets the class value returning True if value was set.
    ''' </summary>
    ''' <param name="value">The value which to set.</param>
    Public Overridable Function TrySetValue(ByVal value As String) As BooleanOutcome

        If GetType(T).Equals(GetType(Double)) Then
        End If
        Dim a As Double
        If System.Double.TryParse(value, Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture, a) Then
            Return Me.TrySetValue(a)
        Else
            Dim synopsis As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                   "{0} failed parsing value.", Me.InstanceName)
            Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                  "{0} failed parsing the value='{1}' to '{2}' type.", Me.InstanceName, value, a.GetType)
            Dim hint As String = "Check the value as it most likely does not represent a number."
            Return New BooleanOutcome(False, synopsis, details, hint)
        End If

    End Function

    ''' <summary>
    ''' Emulates a constructor for this class for setting the specified field value.
    ''' </summary>
    ''' <param name="value">Specifies a new non-null value.</param>
    Public Sub NewField(ByVal value As String)

        Me.NewNull()
        If Not String.IsNullOrWhiteSpace(value) Then
            Me.TrySetValue(value)
        End If

    End Sub

    ''' <summary>
    ''' Emulates a constructor for this class for setting the value to null.
    ''' </summary>
    Public Sub NewNull()
        Me._hasValue = False
    End Sub

    ''' <summary>
    ''' Returns the <see cref="FieldValue">field value</see>.
    ''' </summary>
    Public Overrides Function ToString() As String
        Return Me.FieldValue
    End Function

    ''' <summary>Validates the field value for length.
    ''' </summary>
    Public Function ValidateFieldValue() As BooleanOutcome

        Dim length As Integer
        length = Me.FieldValue.Length
        Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                              " {0} length ('{1}') of formatted ('{2}') Field Value ('{3}') of value ('{4}')",
                                              Me.InstanceName, length, Me.FormatString, Me.FieldValue, Me.Value)
        If Me.MinLength > 0 And length < Me.MinLength Then
            Dim synopsis As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                   "{0} failed validating minimum length.", Me.InstanceName)
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                    "{0} must be at least {1}.", details, Me.MinLength)
            Dim hint As String = "The numeric value may be too small or the numeric field format string incompatible with the expected size."
            Return New BooleanOutcome(False, synopsis, details, hint)
        ElseIf Me.MaxLength > 0 And length > Me.MaxLength Then
            Dim synopsis As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                   "{0} failed validating maximum length.", Me.InstanceName)
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                    "{0} must be at most {1}.", details, Me.MaxLength)
            Dim hint As String = "The numeric value may be too large or the numeric field format string incompatible with the expected size."
            Return New BooleanOutcome(False, synopsis, details, hint)
        Else
            Dim synopsis As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                   "{0} verified.", Me.InstanceName)
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                    "{0} passed length verification.", details)
            Return New BooleanOutcome(True, synopsis, details, String.Empty)
        End If
    End Function

#End Region

End Class
