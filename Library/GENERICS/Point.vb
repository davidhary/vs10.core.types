''' <summary>
''' A Interface definition of point classes.
''' </summary>
''' <typeparam name="T">Specifies the type parameter of the generic class.</typeparam>
Public Interface IPoint(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable})

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="X")>
    Property X() As T

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Y")>
    Property Y() As T
    Function Equals(ByVal value As Object) As Boolean
    Function Equals(ByVal pointCompared As Point(Of T)) As Boolean
    Function GetHashCode() As Int32

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId:="SetPoint")>
    Sub SetPoint(ByVal x As T, ByVal y As T)
    Function ToString() As String

End Interface

''' <summary>
''' Implements a generic point class.
''' </summary>
''' <typeparam name="T">Specifies the type parameter of the generic class.</typeparam>
Public Class Point(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable})

    Implements IPoint(Of T)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Point" /> class.
    ''' </summary>
    Protected Sub New()
        MyBase.New()
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Point" /> class.
    ''' The copy constructor.
    ''' </summary>
    ''' <param name="model">The  <see cref="Point">Point</see> object from which to copy</param>
    Public Sub New(ByVal model As Point(Of T))
        Me.New()
        If model IsNot Nothing Then
            Me._SetPoint(model.X, model.Y)
        End If
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Point" /> class.
    ''' </summary>
    ''' <param name="x">Specifies the X coordinate of the point.</param>
    ''' <param name="y">Specifies the Y coordinate of the point.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    Public Sub New(ByVal x As T, ByVal y As T)
        Me.New()
        Me._SetPoint(x, y)
    End Sub

#End Region

#Region " SHARED "

    ''' <summary> Compares two points. </summary>
    ''' <param name="left">  Specifies the point to compare. </param>
    ''' <param name="right"> Specifies the point to compare with. </param>
    ''' <returns> True if the points are equal. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
    Public Overloads Shared Function Equals(ByVal left As Object, ByVal right As Object) As Boolean
        Return Point(Of T).Equals(TryCast(left, Point(Of T)), TryCast(right, Point(Of T)))
    End Function

    ''' <summary>
    ''' Compares two points.
    ''' </summary>
    ''' <param name="left">Specifies the point to compare.</param>
    ''' <param name="right">Specifies the point to compare with.</param>
    ''' <returns>True if the points are equal.</returns>
    ''' <remarks>
    ''' The two points are the same if they have the same X and Y coordinates.
    ''' </remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
    Public Overloads Shared Function Equals(ByVal left As Point(Of T), ByVal right As Point(Of T)) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return left.X.Equals(right.X) AndAlso left.Y.Equals(right.Y)
        End If
    End Function

    ''' <summary>
    ''' Implements the operator =.
    ''' </summary>
    ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
    ''' <param name="right">Specifies the right hand side argument of the binary operation.</param><returns>
    ''' The result of the operator.
    ''' </returns>
    Public Shared Operator =(ByVal left As Point(Of T), ByVal right As Point(Of T)) As Boolean
        Return ((left Is Nothing) AndAlso (right Is Nothing)) OrElse (left IsNot Nothing) AndAlso left.Equals(right)
    End Operator

    ''' <summary>
    ''' Implements the operator &lt;&gt;.
    ''' </summary>
    ''' <param name="left">Specifies the left hand side argument of the binary operation.</param>
    ''' <param name="right">Specifies the right hand side argument of the binary operation.</param><returns>
    ''' The result of the operator.
    ''' </returns>
    Public Shared Operator <>(ByVal left As Point(Of T), ByVal right As Point(Of T)) As Boolean
        Return ((left Is Nothing) AndAlso (right IsNot Nothing)) OrElse Not ((left IsNot Nothing) AndAlso left.Equals(right))
    End Operator

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    ''' <see cref="T:System.Object" />. </param>
    ''' <returns> true if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, false. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean Implements IPoint(Of T).Equals
        Return obj IsNot Nothing AndAlso (Object.ReferenceEquals(Me, obj) OrElse Point(Of T).Equals(Me, CType(obj, Point(Of T))))
    End Function

    ''' <summary>
    ''' Compares two points.
    ''' </summary>
    ''' <param name="pointCompared">Specifies the point to compare.</param>
    ''' <returns>True if the points are equal.</returns>
    ''' <remarks>
    ''' The two points are the same if they have the same X and Y coordinates.
    ''' </remarks>
    Public Overloads Function Equals(ByVal pointCompared As Point(Of T)) As Boolean Implements IPoint(Of T).Equals
        If pointCompared Is Nothing Then
            Return False
        Else
            Return Point(Of T).Equals(Me, pointCompared)
        End If
    End Function

    ''' <summary>
    ''' Creates a unique hash code.
    ''' </summary>
    ''' <returns>An <see cref="System.Int32">Int32</see> value</returns>
    Public Overloads Overrides Function GetHashCode() As Int32 Implements IPoint(Of T).GetHashCode
        Return Me.X.GetHashCode Xor Me.Y.GetHashCode
    End Function

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary>
    ''' Holds the Y coordinate of the point.
    ''' </summary>
    Public Property Y() As T Implements IPoint(Of T).Y

    ''' <summary>
    ''' Holds the X coordinate of the point.
    ''' </summary>
    Public Property X() As T Implements IPoint(Of T).X

    ''' <summary>
    ''' Sets the point based on the coordinates.
    ''' </summary>
    ''' <param name="x">The x.</param>
    ''' <param name="y">The y.</param>
    Private Sub _SetPoint(ByVal x As T, ByVal y As T)
        If x.CompareTo(Me._x) <> 0 Then
            Me._x = x
        End If
        If y.CompareTo(Me._y) <> 0 Then
            Me._y = y
        End If
    End Sub

    ''' <summary>Sets the point based on the coordinates.</summary>
    ''' <param name="x">Specifies the X coordinate of the point.</param>
    ''' <param name="y">Specifies the Y coordinate of the point.</param>
    ''' <remarks>Use this class to set the point.</remarks>
    Public Sub SetPoint(ByVal x As T, ByVal y As T) Implements IPoint(Of T).SetPoint
        Me.X = x
        Me.Y = y
    End Sub

    ''' <summary>
    ''' Returns the default string representation of the point.
    ''' </summary>
    Public Overrides Function ToString() As String Implements IPoint(Of T).ToString
        Return Point(Of T).ToString(Me.X, Me.Y)
    End Function

    ''' <summary>Returns the default string representation of the point.</summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
    Public Overloads Shared Function ToString(ByVal x As T, ByVal y As T) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "({0},{1})", x, y)
    End Function

#End Region

End Class
